#pragma once

#include <cinttypes>
#include <vector>
#include <unordered_map>
#include <memory>

#include "math/oobb.hpp"
#include "math/matrix.hpp"
#include "occlusion_culling/ocluder.hpp"

#include "utils/culling_type.hpp"


namespace WhirlCulling
{
class CullingObject;
class MultiBVH;
class SoftwareRasterizer;

class CullingSystem final
{
    friend class CullingObject;

public:
    CullingSystem(size_t width, size_t height);
    ~CullingSystem();

    void updateResolution(size_t width, size_t height);

    void setCullingType(CullingType type);

    void registerObject(CullingObject& object, const OOBB& oobb);
    void unregisterObject(CullingObject& object);

    void addOccluder(Occluder* occluder);
    void removeOccluder(Occluder* occluder);

    void setDeltaTime(float deltaTime); // set it before oobb updates

    void resoleVisibility(const Matrix& projection, const Matrix& view);  // call it after oobb updates


private:
    bool isVisible(CullingObject& object);
    void updateOOBB(CullingObject& object, OOBB oobb);

private:
    void validateCachedIndex(CullingObject& object);

private:
    std::uint32_t m_tick = 0;
    std::uint64_t m_nextUniqueId = 0;
    std::uint32_t m_cacheGeneration = 1;  // 0 is reserved for inllavid generation

    std::vector<std::uint32_t> m_visibleTick;
    std::unordered_map<std::uint64_t, std::uint32_t> m_objectMap;
    std::vector<std::uint32_t> m_freeIndexes;
    std::unique_ptr<MultiBVH> m_multiBVH;

    std::vector<Occluder*> m_occluders;
    std::unique_ptr<SoftwareRasterizer> m_occlusionCuller;

    CullingType m_cullingType = CullingType::FrustrumAndOcclusion;
};

}
