#include "oobb_ocluder.hpp"

#include "trianglizer.hpp"

namespace WhirlCulling
{
OobbOcluder::OobbOcluder(const OOBB& oobb)
    : m_oobb(oobb)
{

}

void OobbOcluder::draw(const Matrix &viewProjection, SoftwareRasterizer &device)
{
    auto triangles = Trianglizer::oobb(m_oobb);

    for (auto& triangle : triangles)
    {
        if (auto tr = triangle.tryTransform(viewProjection))
        {
            device.draw(*tr);
        }
    }
}
}

