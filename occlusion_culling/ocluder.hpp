#pragma once

#include "../math/matrix.hpp"
#include "software_rasterizer.hpp"

namespace WhirlCulling
{
class Occluder
{
public:

    virtual ~Occluder() {}

    virtual void draw(const Matrix& viewProjection, SoftwareRasterizer& device) = 0;
};
}


