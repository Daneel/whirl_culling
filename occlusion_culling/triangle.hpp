#pragma once

#include <array>
#include "../math/vector3.hpp"
#include "../math/matrix.hpp"

namespace WhirlCulling
{
class Triangle
{
public:
    using PointContainer = std::array<Vector3, 3>;

public:
    Triangle(const Vector3& firstPoint, const Vector3& secondPoint, const Vector3& thirdPoint)
        : m_points({firstPoint, secondPoint, thirdPoint})
    {

    }

    const PointContainer& points() const
    {
        return m_points;
    }

    std::optional<Triangle> tryTransform(const Matrix& matrix)
    {
        const auto p1 = matrix.tryTrasnsform(m_points[0]);
        const auto p2 = matrix.tryTrasnsform(m_points[1]);
        const auto p3 = matrix.tryTrasnsform(m_points[2]);

        if (p1 && p2 && p3)
        {
            return Triangle(*p1, *p2, *p3);
        }

        return std::nullopt;
    }

    Triangle transform(const Matrix& matrix)
    {
        return Triangle(matrix * m_points[0], matrix * m_points[1], matrix * m_points[2]);
    }


private:
    PointContainer m_points;
};
}

