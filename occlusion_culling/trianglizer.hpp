#pragma once

#include "triangle.hpp"
#include "../math/oobb.hpp"
#include <array>


namespace WhirlCulling
{
class Trianglizer
{
public:

    static std::array<Triangle, 2> plane(const std::array<Vector3, 4>& plane)
    {
        return {Triangle(plane[0], plane[1], plane[2]), Triangle(plane[3], plane[1], plane[2])};
    }

    static std::array<Triangle, 12> oobb(const OOBB& oobb);


};
}
