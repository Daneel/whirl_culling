#pragma once

#include <vector>
#include "triangle.hpp"

void renderScene(void);

namespace WhirlCulling
{
class SoftwareRasterizer final
{

public:
    SoftwareRasterizer(size_t width, size_t height);
    //SoftwareRasterizer(const std::vector<SoftwareRasterizer>& others);

    void draw(const Triangle& triangle);
    bool test(const Triangle& triangle) const;

    bool test(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) const;

    void clear();

private:
    void drawLine(size_t y, const Vector3& start, const Vector3& left, const Vector3& right);
    bool testLine(size_t y, const Vector3& start, const Vector3& left, const Vector3& right) const;

private:
    size_t m_width;
    size_t m_height;
    std::vector<float> m_buffer;
};
}


