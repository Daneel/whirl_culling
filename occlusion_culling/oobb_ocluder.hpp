#pragma once

#include "ocluder.hpp"
#include "../math/oobb.hpp"

namespace WhirlCulling
{
class OobbOcluder
    : public Occluder
{
public:
    OobbOcluder(const OOBB& oobb);

    void draw(const Matrix& viewProjection, SoftwareRasterizer& device) override;

private:
    OOBB m_oobb;
};
}


