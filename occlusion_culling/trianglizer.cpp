#include "trianglizer.hpp"

#include <optional>

namespace WhirlCulling
{

std::array<Triangle, 12> Trianglizer::oobb(const OOBB& oobb)
{
    std::array<std::optional<std::array<Triangle, 2>>, 6> planeTriangles;

    for (size_t i = 0; i < 3; i++)
    {
        for (int sign = -1; sign <= 1; sign += 2)
        {
            const auto planeCenter = oobb.getCenter() + oobb.getDimVectors()[i] * sign;
            const auto firstOther = oobb.getDimVectors()[(i + 1) % 3];
            const auto secondOther = oobb.getDimVectors()[(i + 2) % 3];

            const auto p1 = planeCenter + firstOther + secondOther;
            const auto p2 = planeCenter + firstOther - secondOther;
            const auto p3 = planeCenter - firstOther + secondOther;
            const auto p4 = planeCenter - firstOther - secondOther;

            const auto planeIndex = i * 2 + static_cast<size_t>(sign + 1) / 2;
            planeTriangles[planeIndex].emplace(plane({p1, p2, p3, p4}));
        }
    }

    return {
        planeTriangles[0]->at(0), planeTriangles[0]->at(1),
        planeTriangles[1]->at(0), planeTriangles[1]->at(1),
        planeTriangles[2]->at(0), planeTriangles[2]->at(1),
        planeTriangles[3]->at(0), planeTriangles[3]->at(1),
        planeTriangles[4]->at(0), planeTriangles[4]->at(1),
        planeTriangles[5]->at(0), planeTriangles[5]->at(1),
    };
}
}
