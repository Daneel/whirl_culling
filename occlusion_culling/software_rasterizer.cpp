#include "software_rasterizer.hpp"

#include <algorithm>
#include <cmath>

namespace WhirlCulling
{

SoftwareRasterizer::SoftwareRasterizer(size_t width, size_t height)
    : m_width(static_cast<size_t>(width))
    , m_height(static_cast<size_t>(height))
    , m_buffer(m_height * m_width)
{
    clear();
}

void SoftwareRasterizer::draw(const Triangle& triangle)
{
    const auto maxX = std::max({triangle.points()[0].getX(), triangle.points()[1].getX(), triangle.points()[2].getX()});
    const auto minX = std::min({triangle.points()[0].getX(), triangle.points()[1].getX(), triangle.points()[2].getX()});

    const auto maxY = std::max({triangle.points()[0].getY(), triangle.points()[1].getY(), triangle.points()[2].getY()});
    const auto minY = std::min({triangle.points()[0].getY(), triangle.points()[1].getY(), triangle.points()[2].getY()});

    const auto maxZ = std::max({triangle.points()[0].getZ(), triangle.points()[1].getZ(), triangle.points()[2].getZ()});

    if (maxX < -1.0f || maxY < -1.0f || minX > 1.0f || minY > 1.0f || maxZ < 0)
    {
        return;
    }

    auto p1 = Vector3((triangle.points()[0].getX() + 1) / 2 * m_width,
                      (triangle.points()[0].getY() + 1) / 2 * m_height,
                      triangle.points()[0].getZ());
    auto p2 = Vector3((triangle.points()[1].getX() + 1) / 2 * m_width,
                      (triangle.points()[1].getY() + 1) / 2 * m_height,
                      triangle.points()[1].getZ());
    auto p3 = Vector3((triangle.points()[2].getX() + 1) / 2 * m_width,
                      (triangle.points()[2].getY() + 1) / 2 * m_height,
                      triangle.points()[2].getZ());

    if (p1.getY() > p2.getY())
    {
        std::swap(p1, p2);
    }

    if (p1.getY() > p3.getY())
    {
        std::swap(p1, p3);
    }

    if (p2.getY() > p3.getY())
    {
        std::swap(p2, p3);
    }

    const int topHeight = static_cast<int>(std::floor(p1.getY() + 0.5));
    const int middleHeightTop = static_cast<int>(std::floor(p2.getY() - 0.5));
    const int middleHeightBottom = static_cast<int>(std::floor(p2.getY() + 0.5));
    const int bottomHeight = static_cast<int>(std::floor(p3.getY() - 0.5));


    if (topHeight < static_cast<int>(m_height) && middleHeightTop > 0)
    {
        const int topHeightClamped = std::clamp(topHeight, 0, static_cast<int>(m_height) - 1);
        const int middleHeightTopClamped = std::clamp(middleHeightTop, 0, static_cast<int>(m_height) - 1);

        Vector3 left = p2.getX() < p3.getX() ? p2 : p3;
        Vector3 right = p2.getX() < p3.getX() ? p3 : p2;
        for (int i = topHeightClamped; i <= middleHeightTopClamped; ++i)
        {
            drawLine(static_cast<size_t>(i), p1, left, right);
        }
    }

    if (middleHeightBottom < static_cast<int>(m_height) && bottomHeight > 0)
    {
        const int middleHeightBottomClamped = std::clamp(middleHeightBottom, 0, static_cast<int>(m_height) - 1);
        const int bottomHeightClamped = std::clamp(bottomHeight, 0, static_cast<int>(m_height) - 1);

        Vector3 left = p1.getX() < p2.getX() ? p1 : p2;
        Vector3 right = p1.getX() < p2.getX() ? p2 : p1;
        for (int i = middleHeightBottomClamped; i <= bottomHeightClamped; ++i)
        {
            drawLine(static_cast<size_t>(i), p3, left, right);
        }
    }
}

inline void SoftwareRasterizer::drawLine(size_t y, const Vector3 &start, const Vector3 &left, const Vector3 &right)
{
    float leftX = (y + 0.5f - start.getY()) * (left.getX() - start.getX()) / (left.getY() - start.getY()) + start.getX();
    float leftZ = (y + 0.5f - start.getY()) * (left.getZ() - start.getZ()) / (left.getY() - start.getY()) + start.getZ();

    float rightX = (y + 0.5f - start.getY()) * (right.getX() - start.getX()) / (right.getY() - start.getY()) + start.getX();
    float rightZ = (y + 0.5f - start.getY()) * (right.getZ() - start.getZ()) / (right.getY() - start.getY()) + start.getZ();

    const int startWidth = static_cast<int>(std::floor(leftX + 1));
    const int endWidth = static_cast<int>(std::floor(rightX - 1));


    if (startWidth >= static_cast<int>(m_width) || endWidth < 0 ||
         startWidth >= right.getX() || endWidth <= left.getX() )
    {
        return;
    }



    int startWidthClamped = std::clamp(startWidth, static_cast<int>(std::floor(left.getX()+1)), static_cast<int>(std::floor(right.getX() - 1)));
    int endWidthClamped = std::clamp(endWidth, static_cast<int>(std::floor(left.getX()+1)), static_cast<int>(std::floor(right.getX() - 1)));

    startWidthClamped = std::clamp(startWidthClamped, 0, static_cast<int>(m_width) - 1);
    endWidthClamped = std::clamp(endWidthClamped, 0, static_cast<int>(m_width) - 1);

    float zStep = (rightZ - leftZ) / (endWidth - startWidth);

    float z = leftZ + (startWidthClamped - leftX) * zStep;
    for (int x = startWidthClamped; x <=endWidthClamped; ++x, z += zStep)
    {
        if (z > 0.0f)
        {
            auto& bufZ = m_buffer[y * m_width + static_cast<size_t>(x)];
            bufZ = std::min(z, bufZ);
        }
    }

}


bool SoftwareRasterizer::test(const Triangle& triangle) const
{
    const auto maxX = std::max({triangle.points()[0].getX(), triangle.points()[1].getX(), triangle.points()[2].getX()});
    const auto minX = std::min({triangle.points()[0].getX(), triangle.points()[1].getX(), triangle.points()[2].getX()});

    const auto maxY = std::max({triangle.points()[0].getY(), triangle.points()[1].getY(), triangle.points()[2].getY()});
    const auto minY = std::min({triangle.points()[0].getY(), triangle.points()[1].getY(), triangle.points()[2].getY()});

    const auto maxZ = std::max({triangle.points()[0].getZ(), triangle.points()[1].getZ(), triangle.points()[2].getZ()});

    if (maxX < -1.0f || maxY < -1.0f || minX > 1.0f || minY > 1.0f || maxZ < 0)
    {
        return false;
    }

    auto p1 = Vector3((triangle.points()[0].getX() + 1) / 2 * m_width,
                      (triangle.points()[0].getY() + 1) / 2 * m_height,
                      triangle.points()[0].getZ());
    auto p2 = Vector3((triangle.points()[1].getX() + 1) / 2 * m_width,
                      (triangle.points()[1].getY() + 1) / 2 * m_height,
                      triangle.points()[1].getZ());
    auto p3 = Vector3((triangle.points()[2].getX() + 1) / 2 * m_width,
                      (triangle.points()[2].getY() + 1) / 2 * m_height,
                      triangle.points()[2].getZ());


    if (p1.getY() > p2.getY())
    {
        std::swap(p1, p2);
    }

    if (p1.getY() > p3.getY())
    {
        std::swap(p1, p3);
    }

    if (p2.getY() > p3.getY())
    {
        std::swap(p2, p3);
    }


    const int topHeight = static_cast<int>(std::floor(p1.getY()));
    const int middleHeightTop = static_cast<int>(std::floor(p2.getY() + 1));
    const int middleHeightBottom = static_cast<int>(std::floor(p2.getY()));
    const int bottomHeight = static_cast<int>(std::floor(p3.getY()) + 1);


    if (topHeight < static_cast<int>(m_height) && middleHeightTop > 0)
    {
        const int topHeightClamped = std::clamp(topHeight, 0, static_cast<int>(m_height) - 1);
        const int middleHeightTopClamped = std::clamp(middleHeightTop, 0, static_cast<int>(m_height) - 1);

        Vector3 left = p2.getX() < p3.getX() ? p2 : p3;
        Vector3 right = p2.getX() < p3.getX() ? p3 : p2;
        for (int i = topHeightClamped; i <= middleHeightTopClamped; ++i)
        {
            if (testLine(static_cast<size_t>(i), p1, left, right))
            {
                return true;
            }
        }
    }

    if (middleHeightBottom < static_cast<int>(m_height) && bottomHeight > 0)
    {
        const int middleHeightBottomClamped = std::clamp(middleHeightBottom, 0, static_cast<int>(m_height) - 1);
        const int bottomHeightClamped = std::clamp(bottomHeight, 0, static_cast<int>(m_height) - 1);

        Vector3 left = p1.getX() < p2.getX() ? p1 : p2;
        Vector3 right = p1.getX() < p2.getX() ? p2 : p1;
        for (int i = middleHeightBottomClamped; i <= bottomHeightClamped; ++i)
        {
            if (testLine(static_cast<size_t>(i), p3, left, right))
            {
                return true;
            }
        }
    }

    return false;
}

bool SoftwareRasterizer::test(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) const
{

    if (maxX < -1.0f || maxY < -1.0f || minX > 1.0f || minY > 1.0f || maxZ < 0.0f)
    {
        return false;
    }

    const float innerMinX = (minX + 1) / 2 * m_width;
    const float innerMaxX = (maxX + 1) / 2 * m_width;
    const float innerMinY = (minY + 1) / 2 * m_height;
    const float innerMaxY = (maxY + 1) / 2 * m_height;

    int startX = std::clamp(static_cast<int>(std::floor(innerMinX)), 0, static_cast<int>(m_width) - 1);
    int startY = std::clamp(static_cast<int>(std::floor(innerMinY)), 0, static_cast<int>(m_height) - 1);
    int endX = std::clamp(static_cast<int>(std::floor(innerMaxX + 1)), 0, static_cast<int>(m_width) - 1);
    int endY = std::clamp(static_cast<int>(std::floor(innerMaxY + 1)), 0, static_cast<int>(m_height) - 1);

    int xOffset = startY * m_width;
    for (int y=startY; y<=endY; ++y, xOffset+=m_width)
    {
        for (int x=startX; x<=endX; ++x)
        {
            if (m_buffer[xOffset + x] > minZ)
            {
                return true;
            }
        }
    }

    return false;
}

void SoftwareRasterizer::clear()
{
    for (size_t i=0; i<m_width * m_height; ++i)
    {
        m_buffer[i] = std::numeric_limits<float>::max();
    }
}


inline bool SoftwareRasterizer::testLine(size_t y, const Vector3 &start, const Vector3 &left, const Vector3 &right) const
{
    bool result = false;

    float leftX = (y - start.getY()) * (left.getX() - start.getX()) / (left.getY() - start.getY()) + start.getX();
    float leftZ = (y - start.getY()) * (left.getZ() - start.getZ()) / (left.getY() - start.getY()) + start.getZ();

    float rightX = (y + 1 - start.getY()) * (right.getX() - start.getX()) / (right.getY() - start.getY()) + start.getX();
    float rightZ = (y + 1 - start.getY()) * (right.getZ() - start.getZ()) / (right.getY() - start.getY()) + start.getZ();

    const int startWidth = static_cast<int>(std::floor(leftX));
    const int endWidth = static_cast<int>(std::floor(rightX + 1));


    if (startWidth >= static_cast<int>(m_width) || endWidth < 0
        || startWidth >= right.getX() || endWidth <= left.getX() )
    {
        return false;
    }

    int startWidthClamped = std::clamp(startWidth, static_cast<int>(std::floor(left.getX())), static_cast<int>(std::floor(right.getX() + 1)));
    int endWidthClamped = std::clamp(endWidth, static_cast<int>(std::floor(left.getX())), static_cast<int>(std::floor(right.getX() + 1)));

    startWidthClamped = std::clamp(startWidthClamped, 0, static_cast<int>(m_width) - 1);
    endWidthClamped = std::clamp(endWidthClamped, 0, static_cast<int>(m_width) - 1);

    float zStep = (rightZ - leftZ) / (endWidth - startWidth);

    float z = leftZ + (startWidthClamped - leftX) * zStep;
    float minZ = std::min({start.getZ(), left.getZ(), right.getZ()});
    for (int x = startWidthClamped; x <=endWidthClamped; ++x, z += zStep)
    {
        if (z < minZ)
        {
            z = minZ;
        }

        if (z > 0.0f)
        {
            auto& bufZ = m_buffer[y * m_width + static_cast<size_t>(x)];
            if (bufZ > z)
            {
                return true;
            }
        }
    }

    return false;
}

}
