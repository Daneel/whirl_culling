#include "culling_task.hpp"

#include "../occlusion_culling/trianglizer.hpp"

namespace WhirlCulling
{

namespace
{
inline void getNPVertex(const AABB& aabb, const Plane& plane, Vector3& nVertex, Vector3& pVertex)
{
    if (plane.a > 0.0f)
    {
        nVertex.setX(aabb.getMin().getX());
        pVertex.setX(aabb.getMax().getX());
    }
    else
    {
        nVertex.setX(aabb.getMax().getX());
        pVertex.setX(aabb.getMin().getX());
    }

    if (plane.b > 0.0f)
    {
        nVertex.setY(aabb.getMin().getY());
        pVertex.setY(aabb.getMax().getY());
    }
    else
    {
        nVertex.setY(aabb.getMax().getY());
        pVertex.setY(aabb.getMin().getY());
    }

    if (plane.c > 0.0f)
    {
        nVertex.setZ(aabb.getMin().getZ());
        pVertex.setZ(aabb.getMax().getZ());
    }
    else
    {
        nVertex.setZ(aabb.getMax().getZ());
        pVertex.setZ(aabb.getMin().getZ());
    }
}
}

CullingTask::CullingTask(
        CullingType cullingType,
        const Matrix& projection,
        const Matrix& view,
        BVH &bvh,
        const SoftwareRasterizer &occlusionCuller,
        uint32_t tick,
        CullingTask::VisibleTickList &visibleTicks)

    : m_cullingType(cullingType)
    , m_projectionView(view.transformed() * projection.transformed())
    , m_viewProjection(projection * view)
    , m_bvh(bvh)
    , m_occlusionCuller(occlusionCuller)
    , m_tick(tick)
    , m_visibleTicks(visibleTicks)
{
}

void CullingTask::operator()()
{
    auto planes = PlaneExtractor::extractPlane(m_projectionView);

    if (m_cullingType == CullingType::Occlusion)
    {
        for (auto& object : m_bvh.traverseData())
        {
            cullObject(object.id, object.oobb);
        }
    }
    else
    {
        cullNode(0, planes);
    }
}

inline CullingTask::CullingResult CullingTask::cullAABB(
        const AABB &aabb,
        const FrustrumPlanes &planes,
        uint8_t &clipMask,
        uint8_t &lastClipPlane)
{
    auto& planeIndex = lastClipPlane;
    uint8_t planeMask = INT8_C(1) << planeIndex;

    for (size_t i=0; i < planes.size(); ++i)
    {
        if (clipMask & planeMask)
        {
            auto& plane = planes[planeIndex];

            Vector3 nVertex;
            Vector3 pVertex;

            getNPVertex(aabb, plane, nVertex, pVertex);

            const float pDot = pVertex.getX() * plane.a + pVertex.getY() * plane.b + pVertex.getZ() * plane.c + plane.d;
            if (pDot < 0.0f)
            {
                return CullingTask::CullingResult::OUTSIDE;
            }

            const float nDot = nVertex.getX() * plane.a + nVertex.getY() * plane.b + nVertex.getZ() * plane.c + plane.d;
            if (nDot > 0.0f)
            {
                clipMask ^= planeMask;
            }
            else
            {
                return CullingTask::CullingResult::INTERSECT;
            }
        }
        ++planeIndex;
        planeMask <<= 1;
        if (planeIndex == planes.size())
        {
            planeIndex = 0;
            planeMask = 1;
        }
    }

    return CullingTask::CullingResult::INSIDE;
}

inline void CullingTask::cullOwnObjects(BVHNodeIndex nodeIndex)
{
    auto& traverseData = m_bvh.nodeTraverseData()[nodeIndex];

    if (traverseData.firstSubtreeObject == INVALID_BVH_OBJECT_INDEX)
    {
        return;
    }

    if (traverseData.leftChild != INVALID_BVH_NODE_INDEX)
    {
        auto firstChildSubtreeObject = m_bvh.nodeTraverseData()[traverseData.leftChild].firstSubtreeObject;
        if (firstChildSubtreeObject != traverseData.firstSubtreeObject)
        {
            cullObjectList(traverseData.firstSubtreeObject, firstChildSubtreeObject, false);
        }
    }
    else if (traverseData.rightChild != INVALID_BVH_NODE_INDEX)
    {
        auto firstChildSubtreeObject = m_bvh.nodeTraverseData()[traverseData.rightChild].firstSubtreeObject;
        if (firstChildSubtreeObject != traverseData.firstSubtreeObject)
        {
            cullObjectList(traverseData.firstSubtreeObject, firstChildSubtreeObject, false);
        }
    }
    else
    {
        cullObjectList(traverseData.firstSubtreeObject, traverseData.lastSubtreeObject);
    }
}

inline void CullingTask::cullNode(BVHNodeIndex nodeIndex, const FrustrumPlanes& planes, std::uint8_t clipMask)
{
    if (nodeIndex == INVALID_BVH_NODE_INDEX)
    {
        return;
    }

    auto& traverseData = m_bvh.nodeTraverseData()[nodeIndex];
    auto& aabb = traverseData.aabb;

    CullingResult cullResult = cullAABB(aabb, planes, clipMask, traverseData.lastClipPlane);

    if (cullResult == CullingResult::INSIDE)
    {
        cullObjectList(traverseData.firstSubtreeObject, traverseData.lastSubtreeObject);
    }
    else if (cullResult == CullingResult::INTERSECT)
    {
        cullOwnObjects(nodeIndex);
        cullNode(traverseData.leftChild, planes, clipMask);
        cullNode(traverseData.rightChild, planes, clipMask);
    }
}


inline void CullingTask::cullObjectList(BVHIndex first, BVHIndex last, bool cullLast)
{
    BVHIndex index = first;
    while (index != last)
    {
        auto& objectData = m_bvh.traverseData()[index];
        cullObject(objectData.id, objectData.oobb);
        index = objectData.next;
    }
    if (cullLast)
    {
        auto& objectData = m_bvh.traverseData()[last];
        cullObject(objectData.id, objectData.oobb);
    }
}

inline void CullingTask::cullObject(uint32_t id, const OOBB &oobb)
{
    if (m_cullingType == CullingType::Frustrum)
    {
        m_visibleTicks[id] = m_tick;
        return;
    }

    auto points = oobb.getPoints();
    float minX = std::numeric_limits<float>::max();
    float maxX = std::numeric_limits<float>::lowest();
    float minY = std::numeric_limits<float>::max();
    float maxY = std::numeric_limits<float>::lowest();
    float minZ = std::numeric_limits<float>::max();
    float maxZ = std::numeric_limits<float>::lowest();

    for (auto point : points)
    {
        if (auto transformedPoint = m_viewProjection.tryTrasnsform(point))
        {
            minX = std::min(minX, transformedPoint->getX());
            maxX = std::max(maxX, transformedPoint->getX());
            minY = std::min(minY, transformedPoint->getY());
            maxY = std::max(maxY, transformedPoint->getY());
            minZ = std::min(minZ, transformedPoint->getZ());
            maxZ = std::max(maxZ, transformedPoint->getZ());
        }
        else
        {
            m_visibleTicks[id] = m_tick;
            return;
        }
    }

    if (m_occlusionCuller.test(minX, minY, minZ, maxX, maxY, maxZ))
    {
        m_visibleTicks[id] = m_tick;
    }
}

}
