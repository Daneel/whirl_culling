#pragma once

#include "../frustrum_culling/bvh.hpp"
#include "../occlusion_culling/software_rasterizer.hpp"
#include "../frustrum_culling/plane_extractor.hpp"
#include "culling_type.hpp"

#include <vector>

namespace WhirlCulling
{

class CullingTask
{
    enum class CullingResult
    {
        OUTSIDE,
        INSIDE,
        INTERSECT,
    };

public:
    using OOBBList = std::vector<OOBB>;
    using VisibleTickList = std::vector<std::uint32_t>;

    CullingTask(
            CullingType cullingType,
            const Matrix& projection,
            const Matrix& view,
            BVH& bvh,
            const SoftwareRasterizer& occlusionCuller,
            std::uint32_t tick,
            VisibleTickList& visibleTicks);

    void operator()();

private:
    void cullNode(BVHNodeIndex nodeIndex, const FrustrumPlanes& planes, std::uint8_t clipMask = -1);

    CullingResult cullAABB(const AABB& aabb,
                           const FrustrumPlanes& planes,
                           std::uint8_t& clipMask,
                           std::uint8_t& lastClipPlane);

    void cullOwnObjects(BVHNodeIndex nodeIndex);

    void cullObjectList(BVHIndex first, BVHIndex last, bool cullLast = true);

    void cullObject(std::uint32_t id, const OOBB& oobb);

private:
    CullingType m_cullingType;
    Matrix m_projectionView;
    Matrix m_viewProjection;
    BVH& m_bvh;
    const SoftwareRasterizer& m_occlusionCuller;
    std::uint32_t m_tick;
    VisibleTickList& m_visibleTicks;

};
}
