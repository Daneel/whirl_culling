#pragma once

namespace WhirlCulling
{
enum class CullingType : char
{
    None,
    Frustrum,
    Occlusion,
    FrustrumAndOcclusion,
};
}


