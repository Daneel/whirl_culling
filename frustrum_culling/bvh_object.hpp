#pragma once

#include "../math/aabb.hpp"

#include "id.hpp"


namespace WhirlCulling
{
using BVHObjectRange = std::pair<BVHIndex,  BVHIndex>;

struct BVHObjectTraverseData
{
    BVHObjectTraverseData(std::uint32_t id, const OOBB& oobb)
        : oobb(oobb)
        , id(id)
    {
    }

    OOBB oobb;
    std::uint32_t id = 0;
    BVHIndex next = INVALID_BVH_OBJECT_INDEX;
};

struct BVHObjectTreeData
{
    BVHObjectTreeData(const AABB& aabb)
        : aabb(aabb)
    {
    }

    AABB aabb;
    BVHIndex prev = INVALID_BVH_OBJECT_INDEX;
    BVHNodeIndex nodeIndex = INVALID_BVH_NODE_INDEX;
    BVHIndex prevDynamicObject = INVALID_BVH_OBJECT_INDEX;
    BVHIndex nextDynamicObject = INVALID_BVH_OBJECT_INDEX;
    float becomeStaticTime = 0.0f;
};

}
