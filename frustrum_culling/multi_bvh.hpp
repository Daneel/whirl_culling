#pragma once

#include <vector>

#include "bvh.hpp"

namespace WhirlCulling
{
class MultiBVH final
{
private:
    struct BVHHandle
    {
        BVHIndex internalIndex;
        uint8_t bvhIndex = 255;
    };

public:
    MultiBVH(std::uint8_t bvhCount);

    void addObject(uint32_t id, const OOBB& oobb);
    void updateOOBB(uint32_t id, const OOBB& oobb);

    void setDeltaTime(float deltaTime);
    void update();

    std::vector<BVH>& bvhs() { return m_bvhs; };

private:

    float calculateSAH();
    void extractObjects();
    void pushObjectsDown();
    void build();

private:
    float m_lastSAH = 0.0f;
    std::uint8_t m_perfectBvhCount;
    bool m_builded = false;
    std::vector<BVH> m_bvhs;
    std::vector<BVHHandle> m_objectMap;

    std::vector<BVHObjectTraverseData> m_objectQueue;
    std::vector<BVHIndex> m_prev;
};
}
