#pragma once

#include <cinttypes>
#include <limits>

namespace WhirlCulling
{
using BVHIndex = std::uint32_t;
constexpr BVHIndex INVALID_BVH_OBJECT_INDEX = std::numeric_limits<BVHIndex>::max();

using BVHNodeIndex = std::uint32_t;
constexpr BVHNodeIndex INVALID_BVH_NODE_INDEX = std::numeric_limits<BVHNodeIndex>::max();
}
