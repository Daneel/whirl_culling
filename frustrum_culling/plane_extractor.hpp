#pragma once

#include <array>

#include "../math/plane.hpp"
#include "../math/matrix.hpp"

namespace WhirlCulling
{

using FrustrumPlanes = std::array<Plane, 6>;

class PlaneExtractor
{
public:
    static FrustrumPlanes extractPlane(const Matrix& viewProjection)
    {
        const auto& m = viewProjection.getData();

        FrustrumPlanes result{
            // far plane
            Plane(-m[0][2] + m[0][3],
                  -m[1][2] + m[1][3],
                  -m[2][2] + m[2][3],
                  -m[3][2] + m[3][3]
            ),

            // near plane
            Plane(m[0][2] + m[0][3],
                  m[1][2] + m[1][3],
                  m[2][2] + m[2][3],
                  m[3][2] + m[3][3]
            ),

            // left plane
            Plane(m[0][0] + m[0][3],
                  m[1][0] + m[1][3],
                  m[2][0] + m[2][3],
                  m[3][0] + m[3][3]
            ),

            // right plane
            Plane(-m[0][0] + m[0][3],
                  -m[1][0] + m[1][3],
                  -m[2][0] + m[2][3],
                  -m[3][0] + m[3][3]
            ),

            // up plane
            Plane(-m[0][1] + m[0][3],
                  -m[1][1] + m[1][3],
                  -m[2][1] + m[2][3],
                  -m[3][1] + m[3][3]
            ),

            // down plane
            Plane(m[0][1] + m[0][3],
                  m[1][1] + m[1][3],
                  m[2][1] + m[2][3],
                  m[3][1] + m[3][3]
            ),

        };


        for (size_t i=0; i<6; i++)
        {
            auto ln = Vector3(result[i].a, result[i].b, result[i].c).length();
            result[i].a /= ln;
            result[i].b /= ln;
            result[i].c /= ln;
            result[i].d /= ln;
        }

        return result;
    }
};
}
