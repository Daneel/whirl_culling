#include "bvh.hpp"

#include "sah.hpp"

namespace
{
constexpr float SPLIT_LINE_SIZE_COEF = 0.03f;
constexpr float DYNAMIC_EXPAND_COEF = 0.1f;
constexpr float DYNAMIC_TIME = 2.0f;
}

namespace WhirlCulling
{


BVH::BVH()
    : m_nodeTraverseData(1)
    , m_nodeTreeData(1)
{
}

BVHIndex BVH::addObject(const BVHObjectTraverseData& traverseData)
{
    return addObject(traverseData.id, traverseData.oobb);
}

void BVH::updateOOBB(BVHIndex index, const OOBB &oobb)
{
    m_traverseData[index].oobb = oobb;

    if (m_treeData[index].aabb.contains(AABB(oobb)))
    {
        return;
    }

    removeFromNode(index);

    AABB newAABB = AABB(oobb);
    Vector3 dim = newAABB.getMax() - newAABB.getMin();
    Vector3 speed = (newAABB.getCenter() - m_traverseData[index].oobb.getCenter()) / m_currentDeltaTime;
    AABB speedReserveAABB(newAABB.getMin() + speed * DYNAMIC_TIME, newAABB.getMax() + speed * DYNAMIC_TIME);
    newAABB.expand(speedReserveAABB);
    Vector3 minReserved = newAABB.getMin() - dim * DYNAMIC_EXPAND_COEF;
    Vector3 maxReserved = newAABB.getMin() - dim * DYNAMIC_EXPAND_COEF;

    m_treeData[index].aabb = AABB(minReserved, maxReserved);
    m_treeData[index].becomeStaticTime = m_previosTime + m_currentDeltaTime + DYNAMIC_TIME;
    removeFromDynamicQueue(index);
    addToDynamicQueue(index);
    addObject(index);
}

BVHIndex BVH::addObject(std::uint32_t identifier, const OOBB& oobb)
{
    BVHIndex index;
    if (m_freeBvhIndex.empty())
    {
        m_traverseData.emplace_back(identifier, oobb);
        m_treeData.emplace_back(AABB(oobb));
        index = static_cast<BVHIndex>(m_traverseData.size() - 1);
    }
    else
    {
        index = *--m_freeBvhIndex.end();
        m_freeBvhIndex.pop_back();

        m_traverseData[index] = BVHObjectTraverseData(identifier, oobb);
        m_treeData[index] = BVHObjectTreeData(AABB(oobb));
    }

    addObject(index);
    return index;

}

void BVH::removeObject(BVHIndex index)
{
    removeFromNode(index);
}

void BVH::setDeltaTime(float deltaTime)
{
    m_currentDeltaTime = deltaTime;
}

void BVH::update()
{
    updateObjects();
    updateNodes();
    float time = m_previosTime + m_currentDeltaTime;
    m_previosTime = time;
}

const AABB &BVH::getAABB() const
{
    return m_nodeTraverseData[0].aabb;
}

uint32_t BVH::getObjectCount() const
{
    return static_cast<uint32_t>(m_traverseData.size() - m_freeBvhIndex.size());
}

float BVH::calculateSAH() const
{
    return getAABB().getSurfaceArea() * getObjectCount();
}

void BVH::ensureNodeCreated(BVHNodeIndex parrentIndex, NodeChildType childType)
{
    bool exists = childType == NodeChildType::LEFT
            ? m_nodeTraverseData[parrentIndex].leftChild != INVALID_BVH_NODE_INDEX
            : m_nodeTraverseData[parrentIndex].rightChild != INVALID_BVH_NODE_INDEX;

    if (exists)
    {
        return;
    }

    BVHNodeIndex nodeIndex = INVALID_BVH_NODE_INDEX;
    if (!m_freeNodeIndex.empty())
    {
        nodeIndex = *(m_freeNodeIndex.end()-1);
        m_freeNodeIndex.pop_back();
    }
    else
    {
        m_nodeTraverseData.emplace_back();
        m_nodeTreeData.emplace_back();
        nodeIndex = static_cast<BVHNodeIndex>(m_nodeTraverseData.size() - 1);
    }

    m_nodeTreeData[nodeIndex].parrent = parrentIndex;

    if (childType == NodeChildType::LEFT)
    {
        m_nodeTraverseData[parrentIndex].leftChild = nodeIndex;
    }
    else
    {
        m_nodeTraverseData[parrentIndex].rightChild = nodeIndex;
    }
}

void BVH::removeNode(BVHNodeIndex index)
{
    m_freeNodeIndex.emplace_back(index);

    auto& nodeTraverseData = m_nodeTraverseData[index];
    auto& nodeTreeData = m_nodeTreeData[index];

    nodeTraverseData.aabb = AABB{};
    nodeTraverseData.leftChild = INVALID_BVH_NODE_INDEX;
    nodeTraverseData.rightChild = INVALID_BVH_NODE_INDEX;
    nodeTraverseData.firstSubtreeObject = INVALID_BVH_OBJECT_INDEX;
    nodeTraverseData.lastSubtreeObject = INVALID_BVH_OBJECT_INDEX;

    nodeTreeData.split = std::nullopt;
    nodeTreeData.nextUpdateNode = INVALID_BVH_NODE_INDEX;
    nodeTreeData.prevUpdateNode = INVALID_BVH_NODE_INDEX;
}

void BVH::removeFromUpdateQueue(BVHNodeIndex nodeIndex)
{
    auto& nodeData = m_nodeTreeData[nodeIndex];
    if (nodeData.prevUpdateNode != INVALID_BVH_NODE_INDEX)
    {
        m_nodeTreeData[nodeData.prevUpdateNode].nextUpdateNode = nodeData.nextUpdateNode;
    }
    if (nodeData.nextUpdateNode != INVALID_BVH_NODE_INDEX)
    {
        m_nodeTreeData[nodeData.nextUpdateNode].prevUpdateNode = nodeData.prevUpdateNode;
    }
    if (m_firstUpdateNode == nodeIndex)
    {
        m_firstUpdateNode = nodeData.nextUpdateNode;
    }
    if (m_lastUpdateNode == nodeIndex)
    {
        m_lastUpdateNode = nodeData.prevUpdateNode;
    }
    nodeData.prevUpdateNode = INVALID_BVH_NODE_INDEX;
    nodeData.nextUpdateNode = INVALID_BVH_NODE_INDEX;
}

void BVH::addToUpdateQueue(BVHNodeIndex nodeIndex)
{
    auto& nodeData = m_nodeTreeData[nodeIndex];
    if (m_lastUpdateNode != INVALID_BVH_OBJECT_INDEX)
    {
        auto& lastNodeData = m_nodeTreeData[m_lastUpdateNode];
        lastNodeData.nextUpdateNode = nodeIndex;
        nodeData.prevUpdateNode = m_lastUpdateNode;
        m_lastUpdateNode = nodeIndex;
    }
    else
    {
        m_firstUpdateNode = nodeIndex;
        m_lastUpdateNode = nodeIndex;
    }
}

void BVH::removeFromDynamicQueue(BVHIndex objectIndex)
{
    auto& objectData = m_treeData[objectIndex];
    if (objectData.prevDynamicObject != INVALID_BVH_OBJECT_INDEX)
    {
        m_treeData[objectData.prevDynamicObject].nextDynamicObject = objectData.nextDynamicObject;
    }
    if (objectData.nextDynamicObject != INVALID_BVH_OBJECT_INDEX)
    {
        m_treeData[objectData.nextDynamicObject].prevDynamicObject = objectData.prevDynamicObject;
    }
    if (m_firstDynamicObject == objectIndex)
    {
        m_firstDynamicObject = objectData.nextDynamicObject;
    }
    if (m_lastDynamicObject == objectIndex)
    {
        m_lastDynamicObject = objectData.prevDynamicObject;
    }
    objectData.prevDynamicObject = INVALID_BVH_OBJECT_INDEX;
    objectData.nextDynamicObject = INVALID_BVH_OBJECT_INDEX;
}

void BVH::addToDynamicQueue(BVHIndex objectIndex)
{
    auto& objectData = m_treeData[objectIndex];
    if (m_lastUpdateNode != INVALID_BVH_OBJECT_INDEX)
    {
        auto& lastObjectData = m_treeData[m_lastUpdateNode];
        lastObjectData.nextDynamicObject = objectIndex;
        objectData.prevDynamicObject = m_lastUpdateNode;
    }
    else
    {
        m_lastUpdateNode = objectIndex;
    }
}

void BVH::trySplit(BVHNodeIndex nodeIndex)
{
    auto& nodeTraverseData = m_nodeTraverseData[nodeIndex];
    auto& nodeTreeData = m_nodeTreeData[nodeIndex];
    if (nodeTreeData.split != std::nullopt || isEmpty(nodeIndex))
    {
        return;
    }

    auto listWrapper = BVHListWrapper(m_traverseData, nodeTraverseData.firstSubtreeObject, nodeTraverseData.lastSubtreeObject);
    auto newSplit = SAH::makeSplit(listWrapper, nodeTraverseData.aabb);
    if (!newSplit)
    {
        return;
    }

    const auto axis = newSplit->axis;
    const auto dim = nodeTraverseData.aabb.getMax() - nodeTraverseData.aabb.getMin();
    const float dimSize = dim.getComponent(newSplit->axis);
    const float minLine = nodeTraverseData.aabb.getMin().getComponent(axis);
    const float maxLine = nodeTraverseData.aabb.getMax().getComponent(axis);
    const float leftSize = newSplit->value - minLine;
    const float rightSize = maxLine - newSplit->value;

    const float delta = std::min({
        SPLIT_LINE_SIZE_COEF / 2.0f * leftSize,
        SPLIT_LINE_SIZE_COEF / 2.0f * rightSize,
        SPLIT_LINE_SIZE_COEF * dimSize});

    const float leftValue = newSplit->value - delta;
    const float rightValue = newSplit->value + delta;

    nodeTreeData.split.emplace();
    nodeTreeData.split->axis = newSplit->axis;
    nodeTreeData.split->leftValue = leftValue;
    nodeTreeData.split->rightValue = rightValue;

    BVHIndex index = nodeTraverseData.firstSubtreeObject;
    const BVHIndex afterLast = m_traverseData[nodeTraverseData.lastSubtreeObject].next;

    while (index != afterLast)
    {
        if (m_treeData[index].nodeIndex != nodeIndex)
        {
            break;
        }

        auto& objectData = m_traverseData[index];
        auto& objectTreeData = m_treeData[index];
        BVHIndex nextIndex = objectData.next;

        if (objectTreeData.aabb.getMax().getComponent(axis) < rightValue)
        {
            removeFromNode(index);
            ensureNodeCreated(nodeIndex, NodeChildType::LEFT);
            addToNode(index, m_nodeTraverseData[nodeIndex].leftChild);
        }
        else if (objectTreeData.aabb.getMin().getComponent(axis) > leftValue)
        {
            removeFromNode(index);
            ensureNodeCreated(nodeIndex, NodeChildType::RIGHT);
            addToNode(index, m_nodeTraverseData[nodeIndex].rightChild);
        }
        index = nextIndex;
    }
}

bool BVH::isEmpty(BVHNodeIndex nodeIndex)
{
    return m_nodeTraverseData[nodeIndex].firstSubtreeObject == INVALID_BVH_NODE_INDEX;
}

void BVH::updateNodes()
{
    while (m_firstUpdateNode != INVALID_BVH_NODE_INDEX)
    {
        if (m_firstUpdateNode != 0 && isEmpty(m_firstUpdateNode))
        {
            removeNode(m_firstUpdateNode);
        }
        else
        {
            trySplit(m_firstUpdateNode);
        }
        removeFromUpdateQueue(m_firstUpdateNode);
    }
}

void BVH::updateObjects()
{
    float currentTime = m_previosTime + m_currentDeltaTime;

    while (m_firstDynamicObject != INVALID_BVH_OBJECT_INDEX)
    {
        auto& object = m_treeData[m_firstDynamicObject];
        if (object.becomeStaticTime > currentTime)
        {
            break;
        }

        m_treeData[m_firstDynamicObject].aabb = AABB(m_traverseData[m_firstDynamicObject].oobb);

        removeFromUpdateQueue(m_firstDynamicObject);
    }
}


BVHIndex BVH::getObjectLeftFrom(BVHNodeIndex nodeIndex)
{
    BVHNodeIndex currentNodeIndex = nodeIndex;
    while (m_nodeTreeData[currentNodeIndex].parrent != INVALID_BVH_NODE_INDEX)
    {
        BVHNodeIndex parrentIndex = m_nodeTreeData[currentNodeIndex].parrent;
        auto& parrentData = m_nodeTraverseData[parrentIndex];
        auto comeFrom = parrentData.leftChild == nodeIndex ? NodeChildType::LEFT : NodeChildType::RIGHT;

        if (comeFrom == NodeChildType::RIGHT)
        {
            if (parrentData.lastSubtreeObject != INVALID_BVH_OBJECT_INDEX)
            {
                return parrentData.lastSubtreeObject;
            }
            else
            {
                currentNodeIndex = parrentIndex;
            }
        }
        else
        {
            if (parrentData.rightChild != INVALID_BVH_NODE_INDEX)
            {
                auto& rightNode = m_nodeTraverseData[parrentData.rightChild];
                return m_treeData[rightNode.firstSubtreeObject].prev;
            }
            else if (parrentData.lastSubtreeObject != INVALID_BVH_OBJECT_INDEX)
            {
                return parrentData.lastSubtreeObject;
            }
            else
            {
                currentNodeIndex = parrentIndex;
            }
        }
    }
    return INVALID_BVH_OBJECT_INDEX;
}

BVHIndex BVH::getObjectRightFrom(BVHNodeIndex nodeIndex)
{
    BVHNodeIndex currentNodeIndex = nodeIndex;
    while (m_nodeTreeData[currentNodeIndex].parrent != INVALID_BVH_NODE_INDEX)
    {
        BVHNodeIndex parrentIndex = m_nodeTreeData[currentNodeIndex].parrent;
        auto& parrentData = m_nodeTraverseData[parrentIndex];
        auto comeFrom = parrentData.leftChild == nodeIndex ? NodeChildType::LEFT : NodeChildType::RIGHT;

        if (comeFrom == NodeChildType::RIGHT)
        {
            if (parrentData.lastSubtreeObject != INVALID_BVH_OBJECT_INDEX)
            {
                return m_traverseData[parrentData.lastSubtreeObject].next;
            }
            else
            {
                currentNodeIndex = parrentIndex;
            }
        }
        else
        {
            if (parrentData.rightChild != INVALID_BVH_NODE_INDEX)
            {
                auto& rightNode = m_nodeTraverseData[parrentData.rightChild];
                return rightNode.firstSubtreeObject;
            }
            else if (parrentData.lastSubtreeObject != INVALID_BVH_OBJECT_INDEX)
            {
                return m_traverseData[parrentData.lastSubtreeObject].next;
            }
            else
            {
                currentNodeIndex = parrentIndex;
            }
        }
    }
    return INVALID_BVH_OBJECT_INDEX;
}

void BVH::addObject(BVHIndex objectIndex)
{
    AABB aabb = AABB(m_traverseData[objectIndex].oobb);
    BVHNodeIndex curretNodeIndex = 0;
    while (true)
    {
        auto& nodeData = m_nodeTraverseData[curretNodeIndex];
        nodeData.aabb.expand(aabb);

        removeFromUpdateQueue(curretNodeIndex);
        addToUpdateQueue(curretNodeIndex);

        auto& nodeSplitInfo = m_nodeTreeData[curretNodeIndex].split;
        if (nodeSplitInfo)
        {
            auto axis = nodeSplitInfo->axis;
            if (aabb.getMax().getComponent(axis) < nodeSplitInfo->rightValue)
            {
                ensureNodeCreated(curretNodeIndex, NodeChildType::LEFT);
                curretNodeIndex = nodeData.leftChild;
            }
            else if (aabb.getMin().getComponent(axis) > nodeSplitInfo->leftValue)
            {
                ensureNodeCreated(curretNodeIndex, NodeChildType::RIGHT);
                curretNodeIndex = nodeData.rightChild;
            }
            else
            {
                break;
            }
        }
        else
        {
            break;
        }
    }
    addToNode(objectIndex, curretNodeIndex);
}

void BVH::addToNode(BVHIndex objectIndex, BVHNodeIndex nodeIndex)
{   
    auto& objectTraverseData = m_traverseData[objectIndex];
    auto& objectTreeData = m_treeData[objectIndex];
    auto& nodeTraverseData = m_nodeTraverseData[nodeIndex];

    nodeTraverseData.aabb.expand(objectTreeData.aabb);

    if (nodeTraverseData.firstSubtreeObject != INVALID_BVH_OBJECT_INDEX)
    {

        objectTraverseData.next = nodeTraverseData.firstSubtreeObject;
        objectTreeData.prev = m_treeData[nodeTraverseData.firstSubtreeObject].prev;

        if (m_treeData[nodeTraverseData.firstSubtreeObject].prev != INVALID_BVH_OBJECT_INDEX)
        {
            m_traverseData[m_treeData[nodeTraverseData.firstSubtreeObject].prev].next = objectIndex;
        }
        m_treeData[nodeTraverseData.firstSubtreeObject].prev = objectIndex;
    }
    else
    {
        auto leftIndex = getObjectLeftFrom(nodeIndex);
        if (leftIndex != INVALID_BVH_OBJECT_INDEX)
        {
            objectTraverseData.next = m_traverseData[leftIndex].next;
            objectTreeData.prev = leftIndex;

            if (m_traverseData[leftIndex].next != INVALID_BVH_OBJECT_INDEX)
            {
                m_treeData[m_traverseData[leftIndex].next].prev = objectIndex;
            }
            m_traverseData[leftIndex].next = objectIndex;
        }
        else
        {
            auto rightIndex = getObjectRightFrom(nodeIndex);
            if (rightIndex != INVALID_BVH_OBJECT_INDEX)
            {
                objectTraverseData.next = rightIndex;
                objectTreeData.prev = m_treeData[rightIndex].prev;

                if (m_treeData[rightIndex].prev != INVALID_BVH_OBJECT_INDEX)
                {
                    m_traverseData[m_treeData[rightIndex].prev].next = objectIndex;
                }
                m_treeData[rightIndex].prev = objectIndex;
            }
        }
    }

    updateNodeBoundsOnAdd(nodeIndex, nodeTraverseData.firstSubtreeObject, objectIndex);

    objectTreeData.nodeIndex = nodeIndex;
    nodeTraverseData.aabb.expand(objectTreeData.aabb);

    removeFromUpdateQueue(nodeIndex);
    addToUpdateQueue(nodeIndex);
}

void BVH::removeFromNode(BVHIndex objectIndex)
{
    auto& objectTraverseData = m_traverseData[objectIndex];
    auto& objectTreeData = m_treeData[objectIndex];


    if (objectTreeData.prev != INVALID_BVH_OBJECT_INDEX)
    {
        m_traverseData[objectTreeData.prev].next = objectTraverseData.next;
    }
    if (objectTraverseData.next != INVALID_BVH_OBJECT_INDEX)
    {
        m_treeData[objectTraverseData.next].prev = objectTreeData.prev;
    }

    updateNodeBoundsOnDelete(objectTreeData.nodeIndex, objectIndex);

    objectTreeData.prev = INVALID_BVH_OBJECT_INDEX;
    objectTraverseData.next = INVALID_BVH_OBJECT_INDEX;
    objectTreeData.nodeIndex = INVALID_BVH_NODE_INDEX;
}

void BVH::updateNodeBoundsOnDelete(BVHNodeIndex nodeIndex, BVHIndex objectIndex)
{
    auto& objectTraverseData = m_traverseData[objectIndex];
    auto& objectTreeData = m_treeData[objectIndex];
    auto& nodeTraverseData = m_nodeTraverseData[nodeIndex];

    if (nodeTraverseData.firstSubtreeObject == nodeTraverseData.lastSubtreeObject)
    {
        nodeTraverseData.firstSubtreeObject = INVALID_BVH_OBJECT_INDEX;
        nodeTraverseData.lastSubtreeObject = INVALID_BVH_OBJECT_INDEX;

        if (m_nodeTreeData[nodeIndex].parrent != INVALID_BVH_NODE_INDEX)
        {
            updateNodeBoundsOnDelete(m_nodeTreeData[nodeIndex].parrent, objectIndex);
        }
    }
    else
    {
        if (nodeTraverseData.firstSubtreeObject == objectIndex)
        {
            nodeTraverseData.firstSubtreeObject  = objectTraverseData.next;
            if (m_nodeTreeData[nodeIndex].parrent != INVALID_BVH_NODE_INDEX)
            {
                updateNodeBoundsOnDelete(m_nodeTreeData[nodeIndex].parrent, objectIndex);
            }
        }
        else if (nodeTraverseData.lastSubtreeObject == objectIndex)
        {
            nodeTraverseData.lastSubtreeObject = objectTreeData.prev;
            if (m_nodeTreeData[nodeIndex].parrent != INVALID_BVH_NODE_INDEX)
            {
                updateNodeBoundsOnDelete(m_nodeTreeData[nodeIndex].parrent, objectIndex);
            }
        }
    }
}

void BVH::updateNodeBoundsOnAdd(BVHNodeIndex nodeIndex, BVHIndex oldObjectIndex, BVHIndex newObjectIndex)
{
    auto& nodeTraverseData = m_nodeTraverseData[nodeIndex];

    if (oldObjectIndex == nodeTraverseData.firstSubtreeObject)
    {
        nodeTraverseData.firstSubtreeObject = newObjectIndex;

        if (nodeTraverseData.lastSubtreeObject == INVALID_BVH_NODE_INDEX)
        {
            nodeTraverseData.lastSubtreeObject = newObjectIndex;
        }
    }

    if (nodeTraverseData.leftChild != INVALID_BVH_NODE_INDEX)
    {
        auto leftLastSubtree = m_nodeTraverseData[nodeTraverseData.leftChild].lastSubtreeObject;
        if (leftLastSubtree != INVALID_BVH_NODE_INDEX)
        {
            nodeTraverseData.lastSubtreeObject = leftLastSubtree;
        }
    }
    if (nodeTraverseData.rightChild != INVALID_BVH_NODE_INDEX)
    {
        auto rightLastSubtree = m_nodeTraverseData[nodeTraverseData.rightChild].lastSubtreeObject;
        if (rightLastSubtree != INVALID_BVH_NODE_INDEX)
        {
            nodeTraverseData.lastSubtreeObject = rightLastSubtree;
        }
    }

    if (m_nodeTreeData[nodeIndex].parrent != INVALID_BVH_NODE_INDEX)
    {
        updateNodeBoundsOnAdd(m_nodeTreeData[nodeIndex].parrent, oldObjectIndex, newObjectIndex);
    }
}


}
