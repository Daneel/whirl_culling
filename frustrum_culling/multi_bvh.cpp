#include "multi_bvh.hpp"

#include "bvh_object_list_wrapper.hpp"
#include "sah.hpp"

namespace
{
constexpr float REBUILD_SAH_FACTOR = 0.75;  // rebuild will start when SAH become in 25% worse
}

namespace WhirlCulling
{

namespace
{
class SimpleBVHObjectListWrapper
    : public BVHListWrapper
{
public:

    SimpleBVHObjectListWrapper(BVHObjectTraverseDataContainer& traverseData,
                             std::vector<BVHIndex>& prev,
                             BVHIndex firstIndex,
                             BVHIndex lastIndex)
        : BVHListWrapper(traverseData, firstIndex, lastIndex)
        , m_prev(prev)
    {
    }

    SimpleBVHObjectListWrapper(const SimpleBVHObjectListWrapper& other)
        : BVHListWrapper(other)
        , m_prev(other.m_prev)
    {

    }

    SimpleBVHObjectListWrapper& operator=(const SimpleBVHObjectListWrapper& other)
    {
        m_firstIndex = other.m_firstIndex;
        m_lastIndex = other.m_lastIndex;
        return *this;
    }

    void insert(const Iterator& it)
    {
        if (m_firstIndex == INVALID_BVH_OBJECT_INDEX)
        {
            m_firstIndex = it.index;
            m_lastIndex = it.index;
        }
        else
        {
            m_traverseData[m_lastIndex].next = it.index;
            m_prev[it.index] = m_lastIndex;
            m_lastIndex = it.index;
        }
    }

    void extract(const Iterator& it)
    {
        if (it.index == m_firstIndex)
        {
            m_firstIndex = it->next;
        }
        if (it.index == m_lastIndex)
        {
            m_lastIndex = m_prev[it.index];
        }
        if (it->next != INVALID_BVH_OBJECT_INDEX)
        {
            m_prev[it->next] = m_prev[it.index];
        }
        if (m_prev[it.index] != INVALID_BVH_OBJECT_INDEX)
        {
            m_traverseData[m_prev[it.index]].next = m_traverseData[it.index].next;
        }

        m_prev[it.index] = INVALID_BVH_OBJECT_INDEX;
        m_traverseData[it.index].next = INVALID_BVH_OBJECT_INDEX;

    }

private:
    std::vector<BVHIndex>& m_prev;
};
}


MultiBVH::MultiBVH(uint8_t bvhCount)
    : m_perfectBvhCount(bvhCount)
{
    m_bvhs.resize(bvhCount);
}

void MultiBVH::addObject(std::uint32_t id, const OOBB& oobb)
{
    m_objectQueue.emplace_back(id, oobb);
    m_prev.emplace_back(INVALID_BVH_OBJECT_INDEX);

    auto size = m_objectQueue.size();
    if (size > 1)
    {
        m_objectQueue[size-2].next = static_cast<std::uint32_t>(size - 1);
        m_prev[size-1] = static_cast<std::uint32_t>(size - 2);
    }
    else
    {
        m_prev[0] = INVALID_BVH_OBJECT_INDEX;
    }
    m_objectQueue[size-1].next = INVALID_BVH_OBJECT_INDEX;
}

void MultiBVH::updateOOBB(uint32_t id, const OOBB &oobb)
{
    BVHHandle handle =  m_objectMap[id];
    m_bvhs[handle.bvhIndex].updateOOBB(handle.internalIndex, oobb);
}

void MultiBVH::setDeltaTime(float deltaTime)
{
    for (auto& bvh : m_bvhs)
    {
        bvh.setDeltaTime(deltaTime);
    }
}

void MultiBVH::update()
{
    if (!m_builded)
    {
        build();
        m_builded = true;
        m_lastSAH = calculateSAH();
    }
    else if (calculateSAH() * REBUILD_SAH_FACTOR > m_lastSAH)
    {
        // rebuild
        extractObjects();
        build();
    }
    else
    {
        pushObjectsDown();
    }

    for (auto& bvh : m_bvhs)
    {
        bvh.update();
    }
}

float MultiBVH::calculateSAH()
{
    float result = 0.0f;

    for (const auto& bvh : m_bvhs)
    {
        result += bvh.calculateSAH();
    }

    return result;
}

void MultiBVH::extractObjects()
{
    for (auto& bvh : m_bvhs)
    {
        for (auto& objectData : bvh.traverseData())
        {
            addObject(objectData.id, objectData.oobb);
        }
    }
    m_bvhs.clear();
    m_objectMap.clear();
}

void MultiBVH::pushObjectsDown()
{
    if (m_objectQueue.empty())
    {
        return;
    }

    BVHIndex last = static_cast<BVHIndex>(m_prev.size() - 1);
    SimpleBVHObjectListWrapper listWrapper(m_objectQueue, m_prev, 0, last);
    for (auto& objectData : listWrapper)
    {
        AABB aabb = AABB(objectData.oobb);
        const Vector3 objectCenter = aabb.getCenter();
        bool found = true;
        std::uint8_t minCountIndex = 0;
        float minCenterDist = (m_bvhs[minCountIndex].getAABB().getCenter() - objectCenter).lengthSquared();
        for (size_t i = 0; i < m_bvhs.size(); i++)
        {
            auto& bvh = m_bvhs[i];
            const float distToBvh = (bvh.getAABB().getCenter() - objectCenter).lengthSquared();
            if (distToBvh < minCenterDist)
            {
                minCenterDist = distToBvh;
                minCountIndex = static_cast<std::uint8_t>(i);
            }
            if (bvh.getAABB().contains(aabb))
            {
                found = true;
                bvh.addObject(objectData);
                break;
            }
        }

        if (!found)
        {
            m_bvhs[minCountIndex].addObject(objectData);
        }
    }

    m_objectQueue.clear();
    m_prev.clear();
}

void MultiBVH::build()
{
    m_bvhs.clear();
    m_objectMap.clear();

    if (!m_objectQueue.size())
    {
        return;
    }

    std::vector<AABB> aabbs;
    std::vector<size_t> objectCounts;
    std::vector<SimpleBVHObjectListWrapper> objectLists;

    // calculate parametrs of 1st bvh manually
    aabbs.emplace_back(m_objectQueue[0].oobb);
    for (auto& bvhObjectData : m_objectQueue)
    {
        aabbs[0].expand(AABB(bvhObjectData.oobb));
    }
    objectCounts.push_back(m_objectQueue.size());
    objectLists.emplace_back(m_objectQueue, m_prev, 0, m_objectQueue.size() - 1);

    // make more bvh
    while (m_perfectBvhCount > aabbs.size())
    {
        std::size_t worseBVH = 0;
        float worseSAH = aabbs[0].getSurfaceArea() * objectCounts[0];
        for (std::size_t i = 1; i < aabbs.size(); ++i)
        {
            float sah = aabbs[i].getSurfaceArea() * objectCounts[i];
            if (sah > worseSAH)
            {
                worseSAH = sah;
                worseBVH = i;
            }
        }

        auto split = SAH::makeSplit(objectLists[worseBVH], aabbs[worseBVH]);

        if (!split)
        {
            break;
        }

        //separate objects
        size_t leftCount = objectCounts[worseBVH];
        size_t rightCount = 0;
        SimpleBVHObjectListWrapper leftList(objectLists[worseBVH]);
        SimpleBVHObjectListWrapper rightList(m_objectQueue, m_prev, INVALID_BVH_OBJECT_INDEX, INVALID_BVH_OBJECT_INDEX);
        {
            const auto afterLast = SimpleBVHObjectListWrapper::Iterator(INVALID_BVH_OBJECT_INDEX, m_objectQueue);
            auto it = leftList.begin();

            while (it != afterLast)
            {
                auto next = it;
                ++next;

                Vector3 center = it->oobb.getCenter();
                if (center.getComponent(split->axis) > split->value)
                {
                    --leftCount;
                    ++rightCount;
                    leftList.extract(it);
                    rightList.insert(it);
                }

                it = next;
            }
        }

        if (!leftCount || !rightCount)
        {
            break;
        }

        AABB leftAabb = aabbs[worseBVH];
        leftAabb.acsMax().setComponent(split->axis, split->value);
        AABB rightAabb = aabbs[worseBVH];
        rightAabb.acsMin().setComponent(split->axis, split->value);

        aabbs[worseBVH] = leftAabb;
        objectCounts[worseBVH] = leftCount;
        objectLists[worseBVH] = leftList;

        aabbs.emplace_back(rightAabb);
        objectCounts.emplace_back(rightCount);
        objectLists.emplace_back(rightList);
    }

    for (const auto& list : objectLists)
    {
        m_bvhs.emplace_back();
        for (const auto& object : list)
        {
            auto id = object.id;
            if (id >= m_objectMap.size())
            {
                m_objectMap.resize(id+1);
            }
            BVHHandle handle;
            handle.bvhIndex = static_cast<std::uint8_t>(m_bvhs.size() - 1);
            handle.internalIndex = m_bvhs[m_bvhs.size() - 1].addObject(object);
            m_objectMap[id] = handle;
        }
    }

    m_objectQueue.clear();
    m_prev.clear();
}
}
