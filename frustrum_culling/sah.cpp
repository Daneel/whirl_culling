#include "sah.hpp"


#include <algorithm>


namespace WhirlCulling
{

std::optional<Split> SAH::makeSplit(const BVHListWrapper& list, const AABB& containerAABB)
{
    struct PlaneData
    {
        PlaneData(float value, bool isObjectEnd)
            : value(value)
            , isObjectEnd(isObjectEnd)
        {
        }

        bool operator<(const PlaneData& other)
        {
            return value < other.value;
        }

        float value;
        bool isObjectEnd;
    };

    std::uint32_t objectCount = 0;
    std::array<std::vector<PlaneData>, 3> planes;
    {
        for (const auto& object : list)
        {
            const auto& aabb = AABB(object.oobb);

            planes[0].emplace_back(aabb.getMin().getX(), false);
            planes[0].emplace_back(aabb.getMax().getX(), true);
            planes[1].emplace_back(aabb.getMin().getY(), false);
            planes[1].emplace_back(aabb.getMax().getY(), true);
            planes[2].emplace_back(aabb.getMin().getZ(), false);
            planes[2].emplace_back(aabb.getMax().getZ(), true);

            ++objectCount;
        }
    }

    std::sort(planes[0].begin(), planes[0].end());
    std::sort(planes[1].begin(), planes[1].end());
    std::sort(planes[2].begin(), planes[2].end());

    std::optional<Split> result;
    float bestValue = objectCount * containerAABB.getSurfaceArea();

    for (std::uint8_t axis=0; axis<3; ++axis)
    {
        std::uint32_t leftCount = 0;
        std::uint32_t rightCount = objectCount - 1;
        std::uint32_t middleCount = 1;
        for (std::uint32_t i=0; i<planes[axis].size() - 1; ++i)
        {
            const float splitVal = (planes[axis][i].value + planes[axis][i+1].value) / 2.0f;

            AABB leftAABB = containerAABB;
            AABB rightAABB = containerAABB;

            leftAABB.acsMax().setComponent(axis, splitVal);
            rightAABB.acsMin().setComponent(axis, splitVal);


            const float value = leftAABB.getSurfaceArea() * leftCount
                                + rightAABB.getSurfaceArea() * rightCount
                                + containerAABB.getSurfaceArea() * middleCount;
            if (value < bestValue)
            {
                bestValue = value;
                result.emplace(splitVal, axis);
            }

            if (planes[axis][i].isObjectEnd)
            {
                --middleCount;
                ++leftCount;
            }
            else
            {
                --rightCount;
                ++middleCount;
            }
        }
    }

    return result;
}

}
