﻿#pragma once

#include <cinttypes>
#include <vector>

#include "../math/aabb.hpp"

#include "bvh_object.hpp"
#include "bvh_node.hpp"

namespace WhirlCulling
{
class BVH
{
public:

    BVH();

    BVHIndex addObject(std::uint32_t identifier, const OOBB& oobb);
    BVHIndex addObject(const BVHObjectTraverseData& traverseData);


    void updateOOBB(BVHIndex index, const OOBB& oobb);

    void removeObject(BVHIndex index);

    void setDeltaTime(float deltaTime);
    void update();

    const AABB& getAABB() const;
    std::uint32_t getObjectCount() const;
    float calculateSAH() const;

    std::vector<BVHObjectTraverseData>& traverseData() { return m_traverseData; }
    const std::vector<BVHObjectTraverseData>& traverseData() const { return m_traverseData; }

    std::vector<BVHNodeTraverseData>& nodeTraverseData() { return m_nodeTraverseData; }
    const std::vector<BVHNodeTraverseData>& nodeTraverseData() const { return m_nodeTraverseData; }

private:
    void ensureNodeCreated(BVHNodeIndex parrentIndex, NodeChildType childType);
    void removeNode(BVHNodeIndex index);

    void removeFromUpdateQueue(BVHNodeIndex nodeIndex);
    void addToUpdateQueue(BVHNodeIndex nodeIndex);

    void removeFromDynamicQueue(BVHIndex objectIndex);
    void addToDynamicQueue(BVHIndex objectIndex);

    void trySplit(BVHNodeIndex nodeIndex);

    bool isEmpty(BVHNodeIndex nodeIndex);

    void updateNodes();
    void updateObjects();

    BVHIndex getObjectLeftFrom(BVHNodeIndex nodeIndex);
    BVHIndex getObjectRightFrom(BVHNodeIndex nodeIndex);

    void addObject(BVHIndex objectIndex);
    void addToNode(BVHIndex objectIndex, BVHNodeIndex nodeIndex);
    void removeFromNode(BVHIndex objectIndex);


    void updateNodeBoundsOnDelete(BVHNodeIndex nodeIndex, BVHIndex objectIndex);
    void updateNodeBoundsOnAdd(BVHNodeIndex nodeIndex, BVHIndex oldObjectIndex, BVHIndex newObjectIndex);

private:
    std::vector<BVHObjectTraverseData> m_traverseData;
    std::vector<BVHObjectTreeData> m_treeData;
    std::vector<BVHIndex> m_freeBvhIndex;
    std::vector<BVHNodeTraverseData> m_nodeTraverseData;
    std::vector<BVHNodeTreeData> m_nodeTreeData;
    std::vector<BVHIndex> m_freeNodeIndex;
    float m_previosTime = 0.0f;
    float m_currentDeltaTime = 0.0f;
    BVHIndex m_firstDynamicObject = INVALID_BVH_OBJECT_INDEX;
    BVHIndex m_lastDynamicObject = INVALID_BVH_OBJECT_INDEX;
    BVHIndex m_firstUpdateNode = INVALID_BVH_OBJECT_INDEX;
    BVHIndex m_lastUpdateNode = INVALID_BVH_OBJECT_INDEX;
};
}

