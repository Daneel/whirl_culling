#pragma once

#include <cinttypes>
#include <optional>
#include <vector>

#include "bvh_object_list_wrapper.hpp"


namespace WhirlCulling
{
struct Split
{
    Split(float value, std::uint8_t axis)
        : value(value)
        , axis(axis)
    {}
    float value;
    std::uint8_t axis;
};

class SAH
{
public:
    static std::optional<Split> makeSplit(const BVHListWrapper& list, const AABB& containerAABB);
};
}

