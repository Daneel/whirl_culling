#pragma once

#include <optional>

#include "../math/aabb.hpp"

#include "id.hpp"
#include "bvh_object.hpp"

namespace WhirlCulling
{

enum class NodeChildType : std::uint8_t
{
    LEFT,
    RIGHT,
};


struct SplitInfo
{
    std::uint8_t axis = 0;
    float leftValue = 0.0f;
    float rightValue = 0.0f;
};

struct BVHNodeTraverseData
{
    AABB aabb;
    BVHNodeIndex leftChild = INVALID_BVH_NODE_INDEX;
    BVHNodeIndex rightChild = INVALID_BVH_NODE_INDEX;
    BVHIndex firstSubtreeObject = INVALID_BVH_OBJECT_INDEX;
    BVHIndex lastSubtreeObject = INVALID_BVH_OBJECT_INDEX;
    std::uint8_t lastClipPlane = 0;
};

struct BVHNodeTreeData
{
    std::optional<SplitInfo> split;
    BVHNodeIndex parrent = INVALID_BVH_NODE_INDEX;
    BVHIndex prevUpdateNode = INVALID_BVH_NODE_INDEX;
    BVHIndex nextUpdateNode = INVALID_BVH_NODE_INDEX;
};
}

