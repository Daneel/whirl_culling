#pragma once


#include <vector>

#include "bvh_object.hpp"

namespace WhirlCulling
{
using BVHObjectTraverseDataContainer = std::vector<BVHObjectTraverseData>;

class BVHListWrapper
{
public:
    class Iterator
    {
    public:
        Iterator(BVHIndex index, BVHObjectTraverseDataContainer& traverseData)
            : index(index)
            , m_traverseData(traverseData)
        {}

        Iterator& operator=(const Iterator& other) { index = other.index; return *this; }

        Iterator& operator++(){ index = m_traverseData[index].next; return *this; }
        Iterator operator++(int){ return Iterator(m_traverseData[index].next, m_traverseData); }

        bool operator!= (const Iterator& other) const { return index != other.index; }

        BVHObjectTraverseData& operator* () const { return m_traverseData[index]; }
        BVHObjectTraverseData* operator-> () const { return &m_traverseData[index]; }
    public:
        BVHIndex index;
    private:
        BVHObjectTraverseDataContainer& m_traverseData;
    };


    class ConstIterator
    {
    public:
        ConstIterator(BVHIndex index, const BVHObjectTraverseDataContainer& traverseData)
            : index(index)
            , m_traverseData(traverseData)
        {}

        ConstIterator& operator++(){ index = m_traverseData[index].next; return *this; }
        ConstIterator operator++(int) const { return ConstIterator(m_traverseData[index].next, m_traverseData); }

        bool operator!= (const ConstIterator& other) const { return index != other.index; }

        const BVHObjectTraverseData& operator* () const { return m_traverseData[index]; }
        const BVHObjectTraverseData* operator-> () const { return &m_traverseData[index]; }
    public:
        BVHIndex index;
    private:
        const BVHObjectTraverseDataContainer& m_traverseData;
    };

public:

    BVHListWrapper(BVHObjectTraverseDataContainer& traverseData,
                   BVHIndex firstIndex,
                   BVHIndex lastIndex)
        : m_traverseData(traverseData)
        , m_firstIndex(firstIndex)
        , m_lastIndex(lastIndex)
    {}

    BVHListWrapper(BVHObjectTraverseDataContainer& traverseData,
                   const BVHObjectRange& range)
        : m_traverseData(traverseData)
        , m_firstIndex(range.first)
        , m_lastIndex(range.second)
    {}

    const ConstIterator begin() const
    {
        return ConstIterator(m_firstIndex, m_traverseData);
    }

    Iterator begin()
    {
        return Iterator(m_firstIndex, m_traverseData);
    }

    const ConstIterator end() const
    {
        if (m_lastIndex == INVALID_BVH_OBJECT_INDEX)
        {
            return ConstIterator(INVALID_BVH_OBJECT_INDEX, m_traverseData);
        }
        return ConstIterator(m_traverseData[m_lastIndex].next, m_traverseData);
    }

    Iterator end()
    {
        if (m_lastIndex == INVALID_BVH_OBJECT_INDEX)
        {
            return Iterator(INVALID_BVH_OBJECT_INDEX, m_traverseData);
        }
        return Iterator(m_traverseData[m_firstIndex].next, m_traverseData);
    }

    BVHIndex first()
    {
        return m_firstIndex;
    }

    BVHIndex last()
    {
        return m_lastIndex;
    }

    BVHObjectRange range()
    {
        return BVHObjectRange(m_firstIndex, m_lastIndex);
    }

protected:
    BVHObjectTraverseDataContainer& m_traverseData;
    BVHIndex m_firstIndex;
    BVHIndex m_lastIndex;
};

}

