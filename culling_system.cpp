#include "culling_system.hpp"

#include "culling_object.hpp"
#include "frustrum_culling/multi_bvh.hpp"
#include "occlusion_culling/software_rasterizer.hpp"
#include "utils/culling_task.hpp"

#include <tbb/parallel_for.h>
#include <thread>

namespace
{
constexpr float SOFTWARE_RASTERIZER_SCALE = 0.01f;
}

namespace WhirlCulling
{
CullingSystem::CullingSystem(size_t width, size_t height)
    : m_multiBVH(std::make_unique<MultiBVH>(8))
    , m_occlusionCuller(std::make_unique<SoftwareRasterizer>(width * SOFTWARE_RASTERIZER_SCALE, height * SOFTWARE_RASTERIZER_SCALE))
{

}

CullingSystem::~CullingSystem() = default;

void CullingSystem::updateResolution(size_t width, size_t height)
{
    m_occlusionCuller = std::make_unique<SoftwareRasterizer>(width * SOFTWARE_RASTERIZER_SCALE, height * SOFTWARE_RASTERIZER_SCALE);
}

void CullingSystem::setCullingType(CullingType type)
{
    m_cullingType = type;
}

void CullingSystem::registerObject(CullingObject &object, const OOBB &oobb)
{
    object.m_system = this;
    object.m_uniqueId = m_nextUniqueId;

    std::uint32_t index;
    if (!m_freeIndexes.empty())
    {
        index = (*--m_freeIndexes.end());
        m_freeIndexes.pop_back();
        m_visibleTick[index] = 0;
    }
    else
    {
        index = static_cast<std::uint32_t>(m_visibleTick.size());
        m_visibleTick.emplace_back(0);
    }

    m_objectMap[m_nextUniqueId++] = index;

    m_multiBVH->addObject(index, oobb);
}

void CullingSystem::unregisterObject(CullingObject &object)
{
    object.m_system = nullptr;
    ++m_cacheGeneration;
}

void CullingSystem::setDeltaTime(float deltaTime)
{
    m_multiBVH->setDeltaTime(deltaTime);
}

bool CullingSystem::isVisible(CullingObject &object)
{
    validateCachedIndex(object);
    return m_tick == m_visibleTick[object.m_cachedIndex];
}

void CullingSystem::updateOOBB(CullingObject &object, OOBB oobb)
{
    validateCachedIndex(object);
    m_multiBVH->updateOOBB(object.m_cachedIndex, oobb);
}

void CullingSystem::validateCachedIndex(CullingObject &object)
{
    if (object.m_cacheGeneration != m_cacheGeneration)
    {
        object.m_cachedIndex = m_objectMap[object.m_uniqueId];
        object.m_cacheGeneration = m_cacheGeneration;
    }
}

void CullingSystem::addOccluder(Occluder *occluder)
{
    m_occluders.push_back(occluder);
}

void CullingSystem::removeOccluder(Occluder *occluder)
{
    auto it = std::find(m_occluders.begin(), m_occluders.end(), occluder);
    if (it == m_occluders.end())
    {
        assert(false);
    }
    m_occluders.erase(it);
}

void CullingSystem::resoleVisibility(const Matrix& projection, const Matrix& view)
{
    if (m_cullingType == CullingType::None)
    {
        for (auto& visibleTick : m_visibleTick)
        {
            visibleTick = m_tick;
        }
        return;
    }

    ++m_tick;

    if (m_cullingType == CullingType::Occlusion || m_cullingType == CullingType::FrustrumAndOcclusion)
    {
        m_occlusionCuller->clear();
        Matrix viewProjection = projection * view;
        for (auto occluder : m_occluders)
        {
            occluder->draw(viewProjection, *m_occlusionCuller);
        }
    }

    m_multiBVH->update();


    std::vector<CullingTask> tasks;
    for (auto& bvh : m_multiBVH->bvhs())
    {
        tasks.emplace_back(CullingTask(m_cullingType, projection, view, bvh, *m_occlusionCuller, m_tick, m_visibleTick));
    }
    tbb::parallel_for<size_t>(0, tasks.size(), 1, [&tasks](size_t i)
    {
        tasks[i]();
    });
}

}

