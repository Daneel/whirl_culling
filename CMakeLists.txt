CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
PROJECT(culling)

SET(CMAKE_CXX_STANDARD 20)

SET(OpenGL_GL_PREFERENCE LEGACY)

find_package (Threads)
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS}  ${GLUT_INCLUDE_DIRS} )


#find_package(TBB REQUIRED)
#find_package(TBB REQUIRED)
#set(CMAKE_CXX_FLAGS_DEBUG "-g  -D_DEBUG -D_GLIBCXX_DEBUG")

#find_package(GLM REQUIRED)
#include_directories(${GLM_INCLUDE_DIRS})
#link_directories(${GLM_LIBRARY_DIRS})
#add_definitions(${GLM_DEFINITIONS})

#if(NOT GLM_FOUND)
#        message(Error "GLM not found")
#endif(NOT GLM_FOUND)


SET(SOURCE
    main.cpp
    culling_system.cpp
    culling_object.cpp

    utils/culling_task.cpp

    frustrum_culling/bvh.cpp
    frustrum_culling/bvh_node.cpp
    frustrum_culling/bvh_object.cpp
    frustrum_culling/bvh_object_list_wrapper.cpp
    frustrum_culling/multi_bvh.cpp
    frustrum_culling/sah.cpp

    occlusion_culling/oobb_ocluder.cpp
    occlusion_culling/software_rasterizer.cpp
    occlusion_culling/trianglizer.cpp
)

SET(HEADERS
   culling_system.hpp
   culling_object.hpp

   utils/culling_task.hpp
   utils/culling_type.hpp

   math/oobb.hpp
   math/vector3.hpp
   math/normalized_quaternion.hpp
   math/aabb.hpp
   math/collider.hpp
   math/plane.hpp
   math/matrix.hpp

   frustrum_culling/id.hpp
   frustrum_culling/plane_extractor.hpp
   frustrum_culling/bvh.hpp
   frustrum_culling/bvh_node.hpp
   frustrum_culling/bvh_object.hpp
   frustrum_culling/bvh_object_list_wrapper.hpp
   frustrum_culling/multi_bvh.hpp
   frustrum_culling/sah.hpp

   occlusion_culling/ocluder.hpp
   occlusion_culling/oobb_ocluder.hpp
   occlusion_culling/software_rasterizer.hpp
   occlusion_culling/triangle.hpp
   occlusion_culling/trianglizer.hpp
)

add_executable(${PROJECT_NAME} ${SOURCE} ${HEADERS})
target_link_libraries (${PROJECT_NAME} ${CMAKE_THREAD_LIBS_INIT} ${OPENGL_LIBRARIES} ${GLUT_LIBRARY} tbb)
