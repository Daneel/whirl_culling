#pragma once

#include <array>
#include "vector3.hpp"
#include <optional>

namespace WhirlCulling
{
class Matrix final
{
public:
    using DataType = std::array<std::array<float, 4>, 4>;

    Matrix(const DataType& data) noexcept
        : m_data(data)
    {
    }

    const DataType& getData() const noexcept
    {
        return m_data;
    }

    Vector3 translation() const noexcept
    {
        return Vector3(m_data[0][3], m_data[1][3], m_data[2][3]);
    }

    std::optional<Vector3> tryTrasnsform (const Vector3& point) const noexcept
    {
        const float x = point.getX() * m_data[0][0] + point.getY() * m_data[0][1] + point.getZ() * m_data[0][2] + 1 * m_data[0][3];
        const float y = point.getX() * m_data[1][0] + point.getY() * m_data[1][1] + point.getZ() * m_data[1][2] + 1 * m_data[1][3];
        const float z = point.getX() * m_data[2][0] + point.getY() * m_data[2][1] + point.getZ() * m_data[2][2] + 1 * m_data[2][3];
        const float w = point.getX() * m_data[3][0] + point.getY() * m_data[3][1] + point.getZ() * m_data[3][2] + 1 * m_data[3][3];

        if (w < 0)
        {
            return std::nullopt;
        }

        return Vector3(x / w, y / w, z / w);
    }

    Vector3 operator* (const Vector3& point) const noexcept
    {
        const float x = point.getX() * m_data[0][0] + point.getY() * m_data[0][1] + point.getZ() * m_data[0][2] + 1 * m_data[0][3];
        const float y = point.getX() * m_data[1][0] + point.getY() * m_data[1][1] + point.getZ() * m_data[1][2] + 1 * m_data[1][3];
        const float z = point.getX() * m_data[2][0] + point.getY() * m_data[2][1] + point.getZ() * m_data[2][2] + 1 * m_data[2][3];
        const float w = point.getX() * m_data[3][0] + point.getY() * m_data[3][1] + point.getZ() * m_data[3][2] + 1 * m_data[3][3];
        return Vector3(x / w, y / w, z / w);
    }

    Matrix operator* (const Matrix& other) const noexcept
    {
        DataType result({
            std::array<float, 4>({0.0f, 0.0f, 0.0f, 0.0f}),
            std::array<float, 4>({0.0f, 0.0f, 0.0f, 0.0f}),
            std::array<float, 4>({0.0f, 0.0f, 0.0f, 0.0f}),
            std::array<float, 4>({0.0f, 0.0f, 0.0f, 0.0f}),
        });

        for (size_t i = 0; i < 4; ++i)
        {
            for (size_t ii = 0; ii < 4; ++ii)
            {
                for (size_t iii = 0; iii < 4; ++iii)
                {
                    result[i][ii] += m_data[i][iii] * other.m_data[iii][ii];
                }
            }
        }

        return Matrix(result);
    }

    Matrix transformed() const
    {
        return DataType({
            std::array<float, 4>({m_data[0][0], m_data[1][0], m_data[2][0], m_data[3][0]}),
            std::array<float, 4>({m_data[0][1], m_data[1][1], m_data[2][1], m_data[3][1]}),
            std::array<float, 4>({m_data[0][2], m_data[1][2], m_data[2][2], m_data[3][2]}),
            std::array<float, 4>({m_data[0][3], m_data[1][3], m_data[2][3], m_data[3][3]}),
        });
    }

private:
    DataType m_data;
};


}
