#pragma once

#include <array>
#include "vector3.hpp"
#include "matrix.hpp"

namespace WhirlCulling
{
class OOBB final
{
public:
    using DimVectors = std::array<Vector3, 3>;
    using Points = std::array<Vector3, 8>;

public:

    OOBB() noexcept
    {
    }

    /*OOBB(const Vector3& halfDioganal, const Matrix& matrix) noexcept
    {
        m_center = matrix.translation();
        m_dimVectors[0] = matrix * Vector3(halfDioganal.getX(), 0.0f, 0.0f) - m_center;
        m_dimVectors[1] = matrix * Vector3(0.0f, halfDioganal.getY(), 0.0f) - m_center;
        m_dimVectors[2] = matrix * Vector3(0.0f, 0.0f, halfDioganal.getZ()) - m_center;
    }*/

    OOBB(const Vector3& center, const DimVectors& dimVectors) noexcept
        : m_center(center)
        , m_dimVectors(dimVectors)
    {
    }

    const Vector3& getCenter() const noexcept
    {
        return m_center;
    }

    const DimVectors& getDimVectors() const noexcept
    {
        return m_dimVectors;
    }

    Points getPoints() const
    {
        const auto firstPlaneCenter = m_center + m_dimVectors[0];
        const auto secondPlaneCenter = m_center - m_dimVectors[0];

        return {
            firstPlaneCenter + m_dimVectors[1] + m_dimVectors[0],
            firstPlaneCenter + m_dimVectors[1] - m_dimVectors[0],
            firstPlaneCenter - m_dimVectors[1] + m_dimVectors[0],
            firstPlaneCenter - m_dimVectors[1] - m_dimVectors[0],
            secondPlaneCenter + m_dimVectors[1] + m_dimVectors[0],
            secondPlaneCenter + m_dimVectors[1] - m_dimVectors[0],
            secondPlaneCenter - m_dimVectors[1] + m_dimVectors[0],
            secondPlaneCenter - m_dimVectors[1] - m_dimVectors[0],
        };
    }

private:
    Vector3 m_center;
    DimVectors m_dimVectors;
};
}
