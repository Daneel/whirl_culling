#pragma once

namespace WhirlCulling
{
struct Plane final
{
public:

    Plane(float a, float b, float c, float d) noexcept
        : a(a)
        , b(b)
        , c(c)
        , d(d)
    {
    }

    float a;
    float b;
    float c;
    float d;
};
}
