#pragma once

namespace WhirlCulling
{
enum class PlaneLocation : char
{
    POSITIVE,
    INTERSECT,
    NEGATIVE,
};
}
