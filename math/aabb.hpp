#pragma once

#include "vector3.hpp"
#include "oobb.hpp"
#include <algorithm>

namespace WhirlCulling
{
class AABB final
{
public:

     AABB() noexcept
        : m_min(Vector3(0.0f, 0.0f, 0.0f))
        , m_max(Vector3(0.0f, 0.0f, 0.0f))
    {
    }

    explicit AABB(const Vector3& min, const Vector3& max) noexcept
        : m_min(min)
        , m_max(max)
    {
    }

    explicit AABB(const OOBB& oobb) noexcept
    {
        const auto& dim = oobb.getDimVectors();
        float maxDeltaX = std::max({std::abs(dim[0].getX()), std::abs(dim[1].getX()), std::abs(dim[2].getX())});
        float maxDeltaY = std::max({std::abs(dim[0].getY()), std::abs(dim[1].getY()), std::abs(dim[2].getY())});
        float maxDeltaZ = std::max({std::abs(dim[0].getZ()), std::abs(dim[1].getZ()), std::abs(dim[2].getZ())});

        m_min = Vector3(oobb.getCenter().getX() - maxDeltaX,
                        oobb.getCenter().getY() - maxDeltaY,
                        oobb.getCenter().getZ() - maxDeltaZ);

        m_max = Vector3(oobb.getCenter().getX() + maxDeltaX,
                        oobb.getCenter().getY() + maxDeltaY,
                        oobb.getCenter().getZ() + maxDeltaZ);

    }

    const Vector3& getMin() const noexcept
    {
        return m_min;
    }

    const Vector3& getMax() const noexcept
    {
        return m_max;
    }

    Vector3& acsMin() noexcept
    {
        return m_min;
    }

    Vector3& acsMax() noexcept
    {
        return m_max;
    }

    void setMin(const Vector3& value) noexcept
    {
        m_min = value;
    }

    void setMax(const Vector3& value) noexcept
    {
        m_max = value;
    }

    Vector3 getCenter() const noexcept
    {
        return (m_min + m_max) / 2.0f;
    }

    float getSurfaceArea() const noexcept
    {
        const float w = m_max.getX() - m_min.getX();
        const float h = m_max.getY() - m_min.getY();
        const float d = m_max.getZ() - m_min.getZ();
        return 2 * (w * h + w * d + h * d);
    }

    void expand(const AABB& other) noexcept
    {
        if (empty())
        {
            m_min = other.m_min;
            m_max = other.m_max;
        }
        else
        {
            m_min.setX(std::min(m_min.getX(), other.m_min.getX()));
            m_min.setY(std::min(m_min.getY(), other.m_min.getY()));
            m_min.setZ(std::min(m_min.getZ(), other.m_min.getZ()));

            m_max.setX(std::max(m_max.getX(), other.m_max.getX()));
            m_max.setY(std::max(m_max.getY(), other.m_max.getY()));
            m_max.setZ(std::max(m_max.getZ(), other.m_max.getZ()));
        }
    }

    bool contains(const AABB& other) const noexcept
    {
        return m_min.getX() < other.m_min.getX() &&
               m_min.getY() < other.m_min.getY() &&
               m_min.getZ() < other.m_min.getZ() &&
               m_max.getX() > other.m_max.getX() &&
               m_max.getY() > other.m_max.getY() &&
               m_max.getZ() > other.m_max.getZ();
    }

    bool empty()
    {
        return m_min == m_max;
    }

private:
    Vector3 m_min;
    Vector3 m_max;
};
}
