#pragma once

#include <cinttypes>
#include <cassert>
#include <cmath>

namespace WhirlCulling
{
class Vector3 final
{
public:

    explicit Vector3(float x, float y, float z) noexcept
        : m_x(x)
        , m_y(y)
        , m_z(z)
    {

    }

    Vector3() noexcept
        : Vector3(0.0f, 0.0f, 0.0f)
    {

    }

    float getX() const noexcept
    {
        return m_x;
    }

    float getY() const noexcept
    {
        return m_y;
    }

    float getZ() const noexcept
    {
        return m_z;
    }

    void setX(float value) noexcept
    {
        m_x = value;
    }

    void setY(float value) noexcept
    {
        m_y = value;
    }

    void setZ(float value) noexcept
    {
        m_z = value;
    }

    float getComponent(std::uint8_t axis) const
    {
        switch (axis)
        {
        case 0:
            return m_x;
        case 1:
            return m_y;
        case 2:
            return m_z;
        default:
            assert(false);
        }
    }

    void setComponent(std::uint8_t axis, float value)
    {
        switch (axis)
        {
        case 0:
            m_x = value;
            break;
        case 1:
            m_y = value;
            break;
        case 2:
            m_z = value;
            break;
        default:
            assert(false);
        }
    }

    Vector3 operator+ (const Vector3& other) const noexcept
    {
        return Vector3(this->getX() + other.getX(),
                       this->getY() + other.getY(),
                       this->getZ() + other.getZ());
    }

    Vector3 operator- (const Vector3& other) const noexcept
    {
        return Vector3(this->getX() - other.getX(),
                       this->getY() - other.getY(),
                       this->getZ() - other.getZ());
    }

    Vector3 operator/ (float value) const noexcept
    {
        return Vector3(m_x / value, m_y / value, m_z / value);
    }

    Vector3 operator* (float value) const noexcept
    {
        return Vector3(m_x * value, m_y * value, m_z * value);
    }

    bool operator== (const Vector3& other) const noexcept
    {
        return m_x == other.m_x && m_y == other.m_y && m_z == other.m_z;
    }

    float lengthSquared()
    {
        return m_x * m_x + m_y * m_y + m_z * m_z;
    }

    float length()
    {
        return std::sqrt(lengthSquared());
    }


private:
    float m_x;
    float m_y;
    float m_z;
};
}
