#include <GL/freeglut.h>

#include <chrono>
#include <optional>
#include <sstream>
#include <cmath>

#include "culling_system.hpp"
#include "culling_object.hpp"
#include "occlusion_culling/oobb_ocluder.hpp"
#include "occlusion_culling/trianglizer.hpp"

#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

#include "tbb/task_scheduler_init.h"

namespace
{
float cameraAngle = 0.0f;
float cameraX = 0.0f;
float cameraY = 0.0f;
float cameraZ = 0.0f;
bool doRotateCamera = false;
float lastX = 0.0f;
float lastY = 0.0f;
float deltaX = 0.0f;
float deltaY = 0.0f;
float deltaZ = 0.0f;
float speed = 10.0f;

bool frustrumCulling = true;
bool occlusinCulling = true;


std::optional<WhirlCulling::CullingSystem> cullingSystem;

WhirlCulling::OobbOcluder occluder(WhirlCulling::OOBB(WhirlCulling::Vector3(0, 0, 0),
{WhirlCulling::Vector3(4.8, 0, 0), WhirlCulling::Vector3(0, 4.8, 0), WhirlCulling::Vector3(0, 0, 0.8)}));


float projection[16];
float view[16];


class Object
    : public WhirlCulling::CullingObject
{
public:
    float x = 0.0f;
    float z = 0.0f;
    float radius = 0.0f;
};

std::vector<Object> objects;
}


void initCullingSystem()
{
    for (int i=0; i<200; ++i)
    {
        for (int ii=0; ii<200; ++ii)
        {
            Object obj;
            obj.x = i * 3 - 300;
            obj.z = ii * 3 - 300;
            obj.radius = 0.5;
            WhirlCulling::OOBB oobb(WhirlCulling::Vector3(obj.x, 0.0f, obj.z),
                                    {
                                        WhirlCulling::Vector3(obj.radius, 0.0f, 0.0f),
                                        WhirlCulling::Vector3(0.0f, obj.radius, 0.0f),
                                        WhirlCulling::Vector3(0.0f, 0.0f, obj.radius),
                                    });

            cullingSystem->registerObject(obj, oobb);
            objects.push_back(obj);
        }
    }

    cullingSystem->addOccluder(&occluder);
}

void init()
{

    GLfloat lightModelAmdient[] = {0.05, 0.05, 0.05, 1.0};

    GLfloat lightPosition[] = {100.0, 100.0, 100.0};
    GLfloat lightDiffuse[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat lightSpecular[] = {1.0, 1.0, 1.0};

    //GLfloat matDiffuse[] = {0.0, 0.0, 0.0, 1.0};
    GLfloat matSpecular[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat matShines[] = {50.0};

    glShadeModel(GL_SMOOTH);

    glClearColor(0, 0, 0, 0);

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightModelAmdient);

    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

    //glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR, matSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, matShines);


    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);

    glColorMaterial(GL_FRONT, GL_DIFFUSE);
}

void reshape(int w, int h)
{
    if (!cullingSystem)
    {
        cullingSystem.emplace(w, h);
        initCullingSystem();
    }
    else
    {
        cullingSystem->updateResolution(w, h);
    }

    glViewport(0, 0, (GLsizei)w, (GLsizei)h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat)w / (GLfloat)h, 0.0001, 500.0);

    glMatrixMode(GL_MODELVIEW);
}

void renderScene(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto now = std::chrono::high_resolution_clock::now();
    static std::optional<decltype(now)> lastTime;
    float dt = 0.0f;
    if (lastTime)
    {
        using FloatDuration = std::chrono::duration<float>;
        dt = std::chrono::duration_cast<FloatDuration>(now - *lastTime).count();
    }
    lastTime = now;

    glPushMatrix();

    cameraX += speed * (std::cos(cameraAngle) * deltaX * dt + std::sin(cameraAngle) * deltaZ * dt);
    cameraY += deltaY * dt * speed;
    cameraZ += speed * (std::cos(cameraAngle + 1.57) * deltaX * dt + std::sin(cameraAngle + 1.57) * deltaZ * dt);

    gluLookAt(cameraX + std::sin(cameraAngle), cameraY, cameraZ + std::cos(cameraAngle),
              cameraX, cameraY, cameraZ,
              0.0f, 1.0f, 0.0f);


    glGetFloatv(GL_PROJECTION_MATRIX, projection);
    glGetFloatv(GL_MODELVIEW_MATRIX, view);
    WhirlCulling::Matrix projectionMatrix = WhirlCulling::Matrix::DataType({
        std::array<float, 4>({projection[0], projection[4], projection[8], projection[12]}),
        std::array<float, 4>({projection[1], projection[5], projection[9], projection[13]}),
        std::array<float, 4>({projection[2], projection[6], projection[10], projection[14]}),
        std::array<float, 4>({projection[3], projection[7], projection[11], projection[15]}),
    });
    WhirlCulling::Matrix viewMatrix = WhirlCulling::Matrix::DataType({
        std::array<float, 4>({view[0], view[4], view[8], view[12]}),
        std::array<float, 4>({view[1], view[5], view[9], view[13]}),
        std::array<float, 4>({view[2], view[6], view[10], view[14]}),
        std::array<float, 4>({view[3], view[7], view[11], view[15]}),
    });

    cullingSystem->resoleVisibility(projectionMatrix, viewMatrix);



    std::uint32_t visibleObjects = 0;
    GLUquadricObj *quadObj1;
    quadObj1 = gluNewQuadric ();
    for (auto& object : objects)
    {
        if (object.isVisibleByCulling())
        {
            ++visibleObjects;
            glPushMatrix ();
            glTranslatef(object.x,  0.0f, object.z);
            glColor4f (0.7f, 0.0f, 0.0f, 0.0f);
            gluQuadricDrawStyle (quadObj1, GLU_FILL);
            gluSphere ( quadObj1, static_cast<double>(object.radius), 10, 10);
            glPopMatrix ();
        }
    }
    gluDeleteQuadric (quadObj1);

    glPushMatrix ();
        glScaled(10, 10, 1);
        glColor4f (0, 0, 1, 0);
        glutSolidCube(1);
        ++visibleObjects;
    glPopMatrix ();


    glColor4f (1.0, 1.0, 0, 0);


    glDisable(GL_LIGHTING);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();


    glColor4f (1.0, 1.0, 0, 0);
    {
        glRasterPos2f(-0.9f, 0.9f);
        std::basic_stringstream<char> stream;
        stream << "fps: ";
        stream << 1 / dt;
        glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (unsigned char*) stream.str().c_str());
    }
    {
        glRasterPos2f(-0.9f, 0.8f);
        std::basic_stringstream<char> stream;
        stream << "objects: ";
        stream << objects.size();
        glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (unsigned char*) stream.str().c_str());
    }
    {
        glRasterPos2f(-0.9f, 0.7f);
        std::basic_stringstream<char> stream;
        stream << "visible objects: ";
        stream << visibleObjects;
        glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (unsigned char*) stream.str().c_str());
    }

    {
        glRasterPos2f(-0.9f, 0.6f);
        std::basic_stringstream<char> stream;
        stream << "Frustrum culling: ";
        stream << (frustrumCulling ? "true" : "false");
        glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (unsigned char*) stream.str().c_str());
    }
    {
        glRasterPos2f(-0.9f, 0.5f);
        std::basic_stringstream<char> stream;
        stream << "Occlusion culling: ";
        stream << (occlusinCulling ? "true" : "false");
        glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (unsigned char*) stream.str().c_str());
    }

    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glEnable(GL_LIGHTING);


    glPopMatrix();
    glutSwapBuffers();
}


void keyDown(unsigned char key, int/* x*/, int/* y*/)
{

    switch (key)
    {
        case 'w' : deltaZ = -0.5f; break;
        case 's' : deltaZ = +0.5f; break;
        case 'd' : deltaX = +0.5f; break;
        case 'a' : deltaX = -0.5f; break;
        case 'q' : deltaY = -0.5f; break;
        case 'e' : deltaY = +0.5f; break;

        case 'f':
        {
           frustrumCulling = !frustrumCulling;

           if (!frustrumCulling && !occlusinCulling)
           {
               cullingSystem->setCullingType(WhirlCulling::CullingType::None);
           }
           else if (frustrumCulling && !occlusinCulling)
           {
               cullingSystem->setCullingType(WhirlCulling::CullingType::Frustrum);
           }
           else if (!frustrumCulling)
           {
               cullingSystem->setCullingType(WhirlCulling::CullingType::Occlusion);
           }
           else
           {
               cullingSystem->setCullingType(WhirlCulling::CullingType::FrustrumAndOcclusion);
           }

           break;
        }
        case 'o':
        {
           occlusinCulling = !occlusinCulling;

           if (!frustrumCulling && !occlusinCulling)
           {
               cullingSystem->setCullingType(WhirlCulling::CullingType::None);
           }
           else if (frustrumCulling && !occlusinCulling)
           {
               cullingSystem->setCullingType(WhirlCulling::CullingType::Frustrum);
           }
           else if (!frustrumCulling)
           {
               cullingSystem->setCullingType(WhirlCulling::CullingType::Occlusion);
           }
           else
           {
               cullingSystem->setCullingType(WhirlCulling::CullingType::FrustrumAndOcclusion);
           }

           break;
        }

    }
}

void keyUp(unsigned char key, int/* x*/, int/* y*/) {

        switch (key) {
             case 'w' : deltaZ = 0; break;
             case 's' : deltaZ = 0; break;
            case 'd' : deltaX = 0; break;
            case 'a' : deltaX = 0; break;
            case 'q' : deltaY = 0; break;
            case 'e' : deltaY = 0; break;
        }
}

void mouseButton(int/* button*/, int state, int x, int y)
{
    lastX = x;
    lastY = y;

    doRotateCamera = state == GLUT_DOWN;
}

void mouseMove(int x, int y)
{
    if (doRotateCamera)
    {
        cameraAngle += (x - lastX) * 0.001f;

        lastX = x;
        lastY = y;
    }
}

int main(int argc, char **argv) {

    tbb::task_scheduler_init tbbInit;

    // инициализация
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(400,400);
    glutCreateWindow("WhirlCulling");

    glutDisplayFunc(renderScene);
    glutIdleFunc(renderScene);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyDown);
    glutKeyboardUpFunc(keyUp);
    glutMouseFunc(mouseButton);
    glutMotionFunc(mouseMove);


    init();

    glutMainLoop();

    return 1;
}
