#pragma once

#include <cinttypes>
#include "math/oobb.hpp"
#include "culling_system.hpp"

namespace WhirlCulling
{
class CullingObject
{
    friend class CullingSystem;

public:

    bool isVisibleByCulling()
    {
        return !m_system || m_system->isVisible(*this);
    }

    void updateOOBB(const OOBB& newOOBB)
    {
        m_system->updateOOBB(*this, newOOBB);
    }

private:
    CullingSystem* m_system = nullptr;
    std::uint64_t m_uniqueId = 0;
    std::uint32_t m_cachedIndex = 0;
    std::uint32_t m_cacheGeneration = 0;
};

}
